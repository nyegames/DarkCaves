﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Glide;
using DarkyEvents;

public class HealthType : MonoBehaviour, IUpdateable
{
    private enum ProtectionType
    {
        SHIELD,//Regenerates after a delay, weak to ElementType.ICE
        HEALTH,//Regenerates upto 50% after delay, weak to ElementType.FIRE
    }

    private float _MaxShield, _MaxHealth;

    /// <summary>Shield Protection type, with the value of the current shield</summary>
    public float Shield = 0f;
    /// <summary>Health protection type, with the value of the current</summary>
    public float Health = 1f;

    /// <summary>Dead if health is zero</summary>
    public bool Dead { get { return Health == 0; } }

    public bool WaitingForShieldToRecharge = false;

    /// <summary>Count down before the shield will begin its recharge, if damage is taken this will be reset to ShieldRechargeDelay</summary>
    public float TimeBeforeShieldRechargeStart = 0f;
    /// <summary>Seconds before the shield will begin to recharge again</summary>
    public float ShieldRechargeDelay = 1f;
    /// <summary>Per each 1 shield point, it will take this amount of time to recharge it</summary>
    public float ShieldRechargePerShieldPoint = 0.005f;
    /// <summary>The shield recharge time it would take to recover to MaxShield from your current shield amount</summary>
    public float ShieldRechargeTimeSeconds { get { return (_MaxShield - Shield) * ShieldRechargePerShieldPoint; } }

    private Tweener _ShieldRechargeTweener = new Tweener();

    public void IUpdate(float deltaTime)
    {
        if (Dead) return;

        _ShieldRechargeTweener.Update(deltaTime);

        //If you have recently been hit by damage, then this will be true
        if (WaitingForShieldToRecharge)
        {
            //Reduce the wait til before shields recharge
            TimeBeforeShieldRechargeStart -= deltaTime;
            if (TimeBeforeShieldRechargeStart <= 0f)
            {
                //Begin the recharge behaviour
                TimeBeforeShieldRechargeStart = 0f;
                WaitingForShieldToRecharge = false;
                _ShieldRechargeTweener.Cancel();
                BeginShieldRecharge();
                DarkyEvents.EventManager.TriggerEvent(EventNames.RechargeShieldStart, new { MaxShield = _MaxShield, ShieldRechargeTime = ShieldRechargeTimeSeconds }, gameObject.IsMine());
            }
        }
    }

    public void BeginShieldRecharge()
    {
        _ShieldRechargeTweener.Tween(this, new { Shield = _MaxShield }, ShieldRechargeTimeSeconds);
    }

    /// <summary>Apply damage to this health type, first remove shield, then armor then health</summary>
    /// <param name="damage"></param>
    /// <returns></returns>
    public bool ApplyDamage(DamageType damage)
    {
        float shieldBefore = Shield;
        float damageValue = damage.DamageValue;
        ApplyDamagePortion(ref damageValue, ref Shield);
        ApplyDamagePortion(ref damageValue, ref Health);

        //Disable any rechaging shields
        _ShieldRechargeTweener.Cancel();

        TimeBeforeShieldRechargeStart = ShieldRechargeDelay;
        WaitingForShieldToRecharge = true;

        return Dead;
    }

    /// <summary>Applies the damage to the particular health type, any left over damage will be stored</summary>
    /// <param name="damage"></param>
    /// <param name="healthType"></param>
    private void ApplyDamagePortion(ref float damage, ref float healthType)
    {
        if (healthType <= 0) return;
        healthType -= damage;
        damage = 0;
        if (healthType >= 0) return;
        damage = Math.Abs(healthType);
        healthType = 0;
    }

    public void RegisterHealthType(float startingShield, float startingArmor, float startingHealth)
    {
        Shield = 0; Health = 0;
        ApplyStartingValues(startingShield, startingHealth);
    }

    public void RegisterHealthType(CharacterStats characterStats)
    {
        Shield = 0; Health = 0;
        ApplyStartingValues(characterStats.BaseShield, characterStats.BaseHealth);
    }

    private void ApplyStartingValues(float startingShield, float startingHealth)
    {
        Shield = startingShield;
        Health = startingHealth;

        _MaxShield = startingShield;
        _MaxHealth = startingHealth;
    }
}
