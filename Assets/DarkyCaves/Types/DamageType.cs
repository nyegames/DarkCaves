﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct DamageType
{
    public enum ElementType
    {
        NORMAL,//Normal damage, x1 effective against everything
        ICE,//Slows, weak against health and armor, x2 earth, x0.5 fire
        FIRE,//Burns health weak against armor and shield, x2 ice, x0.5 earth
        EARTH//AOE splash from target, weak against health and shield, x2 fire, x0.5 ice
    }

    public ElementType ElementalType;

    /// <summary>The raw damage value which is to be applied to the receiving source of this attack</summary>
    public float DamageValue;

    public DamageType(ElementType elementType, float damageValue)
    {
        ElementalType = elementType;
        DamageValue = damageValue;
    }
}
