﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WobbleSprite : MonoBehaviour
{
    private enum ScaleState
    {
        UP,
        DOWN
    }

    private ScaleState CurrentState = ScaleState.UP;

    private SpriteRenderer _Sprite;
    private float _Rot = 0;

    private float _StartingScale;

    public float ScaleSpeed;
    public float MaxScalePercent;
    public float MinScalePercent;
    public float RotatingSpeed;

    // Use this for initialization
    void Start()
    {
        _Sprite = GetComponent<SpriteRenderer>();
        _StartingScale = transform.transform.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        _Rot = Time.deltaTime * RotatingSpeed;
        _Sprite.transform.Rotate(0, 0, _Rot);

        float scaleSpeed = Time.deltaTime * ScaleSpeed;
        switch (CurrentState)
        {
            case ScaleState.UP:
                _Sprite.transform.localScale += Vector3.one * scaleSpeed;
                break;
            case ScaleState.DOWN:
                _Sprite.transform.localScale -= Vector3.one * scaleSpeed;
                break;
        }
        float scaleX = _Sprite.transform.localScale.x;
        if (CurrentState == ScaleState.UP && scaleX > (_StartingScale * MaxScalePercent)) CurrentState = ScaleState.DOWN;
        if (CurrentState == ScaleState.DOWN && scaleX < (_StartingScale * MinScalePercent)) CurrentState = ScaleState.UP;
    }
}
