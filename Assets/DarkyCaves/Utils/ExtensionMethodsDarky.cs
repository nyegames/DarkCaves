﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

public static class ExtensionMethodsDarky
{
    public static Vector2 AbsPosition(this Vector2 pos)
    {
        return new Vector2(Mathf.Abs(pos.x), Mathf.Abs(pos.y));
    }

    public static T GetCopyOf<T>(this Component comp, T other) where T : Component
    {
        Type type = comp.GetType();
        if (type != other.GetType()) return null; // type mis-match
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos)
        {
            if (pinfo.CanWrite)
            {
                try
                {
                    pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                }
                catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos)
        {
            finfo.SetValue(comp, finfo.GetValue(other));
        }
        return comp as T;
    }

    /// <summary>Returns a list of all attached components which contain a script that inherits from the given Type</summary>
    /// <param name="gameObject">has to be invoked from a GameObject</param>
    /// <param name="ofType">Type of the Component you are looking for</param>
    /// <param name="ignoreDisabled">If true, and the Component is of Monobehaviour, if the Monobehaviour is disabled, it will ignore it in the returned List</param>
    /// <returns></returns>
    public static IList GetAllAttachedTypes(this GameObject gameObject, Type ofType, bool ignoreDisabled)
    {
        var findType = typeof(List<>);
        var constructiveListType = findType.MakeGenericType(ofType);
        var returnList = (IList)Activator.CreateInstance(constructiveListType);

        foreach (Component comp in gameObject.GetComponents<Component>())
        {
            if (!ofType.IsAssignableFrom(comp.GetType())) continue;
            if (ignoreDisabled)
            {
                MonoBehaviour monoB = comp as MonoBehaviour;
                if (monoB == null || !monoB.enabled) continue;
            }
            returnList.Add(comp);
        }
        return returnList;
    }

    public static bool IsMine(this GameObject gameObject)
    {
        CharacterMainComponent mainComp = gameObject.GetComponent<CharacterMainComponent>();
        return mainComp != null && mainComp.photonView.isMine;
    }

    /// <summary>Grab all of the object type you choose from inside a Class Type you choose, then set the flags you want to search for
    /// defaults are Public and Static fields</summary>
    /// <param name="obj"></param>
    /// <param name="classType"></param>
    /// <param name="searchType"></param>
    /// <param name="bindingFlags"></param>
    /// <returns></returns>
    public static IEnumerable<MemberInfo> AllOfType(this System.Object obj, Type classType, Type searchType, BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Static, Boolean fields = true)
    {
        if (fields)
        {
            FieldInfo[] groupClass = classType.GetFields(bindingFlags);
            IEnumerable<FieldInfo> filterFields = groupClass.Where(s => s.FieldType.Equals(searchType));
            return filterFields as IEnumerable<MemberInfo>;
        }
        else
        {
            PropertyInfo[] groupClass = classType.GetProperties(bindingFlags);
            IEnumerable<PropertyInfo> filterProperties = groupClass.Where(s => s.PropertyType.Equals(searchType));
            return filterProperties as IEnumerable<MemberInfo>;
        }
    }

    /// <summary>When passing in anonymous types into an Object like new {Power = 100}
    /// You can grab that value back out by using this
    /// obj.ValFromObject("Power");</summary>
    /// <param name="obj"></param>
    /// <param name="valueName"></param>
    /// <returns></returns>
    public static System.Object ValFromObject(this System.Object obj, string valueName)
    {
        System.Type t = obj.GetType();
        PropertyInfo p = t.GetProperty(valueName);
        if (p == null) return null;
        return p.GetValue(obj, null);
    }

    public static GameObject GetChildWithName(this Transform trans, string name)
    {
        if (trans.gameObject.name.Equals(name)) return trans.gameObject;
        for (int i = 0; i < trans.childCount; i++)
        {
            if (trans.GetChild(i).name.Equals(name)) return trans.GetChild(i).gameObject;
        }
        return null;
    }

    /// <summary>Allows to get a game object from its children</summary>
    /// <param name="gameObj"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public static GameObject GetChildWithName(this GameObject gameObj, string name)
    {
        if (gameObj.name.Equals(name)) return gameObj;

        for (int i = 0; i < gameObj.transform.childCount; i++)
        {
            if (gameObj.transform.GetChild(i).name.Equals(name)) return gameObj.transform.GetChild(i).gameObject;
        }
        return null;
    }
}
