﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DestroyAnimationAfterPlay : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        if (GetComponent<Animator>() == null) Destroy(this);
        Destroy(gameObject, GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).LongLength);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
