﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LockObjectPositionsTogether : MonoBehaviour
{
    public Transform LockPositonToThis;
    private float Z;

    public Vector3 StartingOffset;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector3(LockPositonToThis.position.x, LockPositonToThis.position.y, 0) + StartingOffset;
    }
}
