﻿using Glide;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class ProgressBar : MonoBehaviour
{
    private RectTransform _RectTransform;

    private float _CurrentBarSize;

    private float _BarSize;
    public float BarSize
    {
        get
        {
            return _BarSize;
        }
        set
        {
            _BarSize = value;
            _RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, BarSize);
        }
    }

    private Tweener _TweenBar = new Tweener();

    // Use this for initialization
    void Start()
    {
        _RectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        _TweenBar.Update(Time.deltaTime);
    }

    public void SetBarValue(float newValue, float timeTaken = 0f, System.Action finishAction = null)
    {
        _TweenBar.Cancel();
        Tween twn = _TweenBar.Tween(this, new { BarSize = newValue }, timeTaken);
        if (finishAction == null) return;
        twn.OnComplete(finishAction);
    }
}
