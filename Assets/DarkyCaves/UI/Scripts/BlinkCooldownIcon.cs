﻿using DarkyEvents;
using Glide;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkCooldownIcon : MonoBehaviour
{
    /// <summary>The icon image used by this CooldownIcon</summary>
    private Image IconImage;
    /// <summary>The Mask image used to show the radial cooldown time</summary>
    private Image MaskCooldownImage;

    /// <summary>The Icon to show in this cooldown icon</summary>
    public Sprite IconSprite;

    private Tweener _Tweener = new Tweener();

    // Use this for initialization
    void Start()
    {
        IconImage = GetComponent<Image>();
        MaskCooldownImage = transform.GetChild(0).GetComponent<Image>();
        MaskCooldownImage.enabled = false;

        IconImage.sprite = IconSprite;

        DarkyEvents.EventManager.StartListening(EventNames.BlinkTriggered, this.StartBlinkCooldown);
        DarkyEvents.EventManager.StartListening(EventNames.HubPauseMenu, this.RestartBlinkCooldown);
    }

    private void RestartBlinkCooldown(System.Object paused)
    {
        if ((bool)paused) return;
        StartBlinkCooldown(0f);
    }

    public void StartBlinkCooldown(System.Object obj)
    {
        _Tweener.Cancel();
        _Tweener.Tween(MaskCooldownImage, new { fillAmount = 0 }, (float)obj).OnComplete(() =>
        {
            MaskCooldownImage.enabled = false;
            MaskCooldownImage.fillAmount = 1f;
        });
        MaskCooldownImage.enabled = true;
    }

    private void OnDestroy()
    {
        DarkyEvents.EventManager.StopListening(EventNames.BlinkTriggered, this.StartBlinkCooldown);
        DarkyEvents.EventManager.StopListening(EventNames.HubPauseMenu, this.RestartBlinkCooldown);
    }

    // Update is called once per frame
    void Update()
    {
        _Tweener.Update(Time.deltaTime);
    }
}
