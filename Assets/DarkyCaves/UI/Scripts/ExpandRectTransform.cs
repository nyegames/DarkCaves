﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class ExpandRectTransform : MonoBehaviour
{
    private static int _MenusOpen = 0;

    /// <summary>The width to expand to when clicked</summary>
    public float WidthToExpandTo;
    /// <summary>The height to expand to when clicked</summary>
    public float HeightToExpandTo;

    public float ScaleX, ScaleY;

    /// <summary>Speed at which the animation takes ?? Yah i dunno</summary>
    public float AnimSpeed = 1000.0f;

    /// <summary>True if the animation is occuring</summary>
    private bool _Animating = false;

    /// <summary>True if the UI is in the process of being resized</summary>
    private bool _Resizing;
    public bool Opening
    {
        get
        {
            return _Resizing;
        }
        set
        {
            _Resizing = value;
            if (value)
            {
                _EndSize = new Vector2(WidthToExpandTo, HeightToExpandTo);
            }
            else
            {
                _EndSize = _StartingSize;
            }
            _StartTime = Time.time;
            _CurrentSize = new Vector2(_RectTransform.rect.width, _RectTransform.rect.height);
            _Length = Vector2.Distance(_CurrentSize, _EndSize);
        }
    }

    public void ForceOpen()
    {
        //If you are opening already and animating then ignore this
        if (Opening && _Animating) return;
        ToggleResizing();
    }

    /// <summary>Toggle to trigger the resizing of the button animation</summary>
    public void ToggleResizing()
    {
        _Animating = true;
        Opening = !Opening;

        _BehaviourChildren = GetComponentsInChildren<Behaviour>();
        List<Behaviour> behavChildren = new List<Behaviour>(_BehaviourChildren);
        List<Behaviour> temp = new List<Behaviour>();
        foreach (Behaviour bc in behavChildren)
        {
            //Don't add this object to deactivate/activate upon open/closing otherwise it gets stuck
            if (bc == this) continue;
            //These Types are the ones you want to hide/unhide when the transition happens
            if (bc is Text)
            {
                temp.Add(bc);
            }
        }
        _BehaviourChildren = temp.ToArray();
        foreach (Behaviour bchild in _BehaviourChildren) bchild.enabled = false;

        _ChildrenGameObjects = GetComponentsInChildren<Component>(true).OfType<GameObject>().ToArray();
        foreach (GameObject gObj in _ChildrenGameObjects) gObj.SetActive(false);

        //If you are closing a menu, then you don't need to increment the counter
        if (!Opening) return;
        _MenusOpen++;
    }

    private Behaviour[] _BehaviourChildren;
    private GameObject[] _ChildrenGameObjects;

    /// <summary>UI Transform</summary>
    private RectTransform _RectTransform;
    /// <summary>Original small size of the object</summary>
    private Vector2 _StartingSize;
    /// <summary>The currently end size you wish to animate to</summary>
    private Vector2 _EndSize;
    /// <summary>The current size of the object, when triggering the animation</summary>
    private Vector2 _CurrentSize;

    /// <summary>The time to taken to get from Start->End</summary>
    private float _StartTime;
    /// <summary>The distance between Start->End points</summary>
    private float _Length;

    // Use this for initialization
    void Start()
    {
        _RectTransform = GetComponent<RectTransform>();
        _StartingSize = new Vector2(_RectTransform.rect.width, _RectTransform.rect.height);
        _RectTransform = GetComponent<RectTransform>();
        ExtendedStart();

        if (ScaleX > 0 || ScaleY > 0)
        {
            WidthToExpandTo = _RectTransform.rect.width * ScaleX;
            HeightToExpandTo = _RectTransform.rect.height * ScaleY;
        }
    }

    protected virtual void ExtendedStart()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!_Animating) return;

        float dist = (Time.time - _StartTime) * AnimSpeed;
        float frac = dist / _Length;

        _RectTransform.sizeDelta = Vector2.Lerp(_CurrentSize, _EndSize, frac);

        if (frac >= 1)
        {
            _Animating = false;

            //If the first text begins with "ShrinkView_" then !_Resizing its enabled
            for (int i = 0; i < _BehaviourChildren.Length; i++)
            {
                bool enabledMe = _Resizing;
                if (i == 0 && _BehaviourChildren[0].name.StartsWith("ShrinkView_")) enabledMe = !enabledMe;
                _BehaviourChildren[i].enabled = enabledMe;
            }

            foreach (GameObject gObj in _ChildrenGameObjects) gObj.SetActive(_Resizing);

            //If you are opening a menu, then you don't want to decrement the open menu counter
            if (_Resizing) return;
            _MenusOpen--;
            //If you have any more menus open, then you can't allow clicking yet
            if (_MenusOpen > 0) return;
            //Game should be activate now
        }
    }

    private float lerp(float a, float b, float t)
    {
        return (1 - t) * a + t * b;
    }
}
