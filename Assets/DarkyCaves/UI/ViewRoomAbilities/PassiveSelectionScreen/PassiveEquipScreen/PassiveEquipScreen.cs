﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassiveEquipScreen : MonoBehaviour
{
    public int PassiveIndexSelected = -1;

    public GameObject PassiveSelectionScreen;
    public GameObject PassiveEquipScreenScrollable;

    private void OnEnable()
    {
        PassiveSelectionScreen = GameObject.Find("PassiveSelectionScreen");
        PassiveEquipScreenScrollable = GameObject.Find("PassiveEquipScreenScrollable");
    }

    public void OnBackPressed()
    {
        gameObject.SetActive(false);
        PassiveSelectionScreen.SetActive(true);
    }
}
