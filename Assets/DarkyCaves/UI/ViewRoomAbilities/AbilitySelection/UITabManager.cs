﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITabManager : MonoBehaviour
{
    /// <summary>These are the tabs that are children of this manager</summary>
    public UITab[] ManagedTabs;
    /// <summary>The current tab that is active</summary>
    public UITab CurrentTab;

    /// <summary>These are the buttons which once clicked will open the assosicated UITab</summary>
    public Dictionary<UITab, Button> TabOpenTriggers = new Dictionary<UITab, Button>();
    /// <summary>The prefab used for the TabOpen button</summary>
    public GameObject TabOpenButtonPrefab;

    // Use this for initialization
    void OnEnable()
    {
        ManagedTabs = GetComponentsInChildren<UITab>();

        if (ManagedTabs.Length <= 0) return;
        if (CurrentTab == null) CurrentTab = ManagedTabs[0];

        foreach (UITab tab in ManagedTabs)
        {
            tab.gameObject.SetActive(CurrentTab.Equals(tab));
            TabOpenTriggers.Add(tab, CreateNewTabButton(tab.name));
        }

        foreach (KeyValuePair<UITab, Button> pair in TabOpenTriggers)
        {
            UITab tabPressed = pair.Key;
            pair.Value.onClick.AddListener(() =>
            {
                TabPressed(tabPressed);
            });
        }
        TabPressed(CurrentTab);
    }

    private void OnDisable()
    {
        foreach (Button but in TabOpenTriggers.Values)
        {
            Destroy(but.gameObject);
        }
        TabOpenTriggers.Clear();
        if (ManagedTabs == null) return;
        foreach (UITab tab in ManagedTabs)
        {
            tab.gameObject.SetActive(true);
        }
    }

    private Button CreateNewTabButton(string name)
    {
        RectTransform container = GetComponent<RectTransform>();

        GameObject newTabButton = Instantiate(TabOpenButtonPrefab);
        RectTransform tabRectT = newTabButton.GetComponent<RectTransform>();
        tabRectT.transform.parent = transform;
        tabRectT.localScale = Vector3.one;
        tabRectT.anchoredPosition = new Vector2(TabOpenTriggers.Count * (tabRectT.sizeDelta.x * 1.01f), tabRectT.sizeDelta.y * 1.01f);

        newTabButton.GetComponentInChildren<Text>().text = name;

        return newTabButton.GetComponent<Button>();
    }

    private void TabPressed(UITab pressedTab)
    {
        CurrentTab = pressedTab;
        foreach (KeyValuePair<UITab, Button> pair in TabOpenTriggers)
        {
            bool current = pair.Key.Equals(pressedTab);
            pair.Key.gameObject.SetActive(current);
            Color col = !current ? new Color(100f / 255f, 100f / 255f, 100f / 255f, 255f / 255f) : Color.white;
            pair.Value.GetComponent<Image>().color = col;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
