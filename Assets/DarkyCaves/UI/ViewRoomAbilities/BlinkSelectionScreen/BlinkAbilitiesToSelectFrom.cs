﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BlinkAbilitiesToSelectFrom : UIVerticalList
{
    public RoomAndPassiveUIController ViewRoomController;

    protected override List<string> PopulateInformationList()
    {
        ExtraBufferBetweenItems = 10f;
        ViewRoomController = FindObjectOfType<RoomAndPassiveUIController>();
        return SQLPlayerInfo.SQLManager.Instance.GetPlayerUnlockedBlinks(ViewRoomController.CurrentSelectedPlayerName).ToList();
    }

    protected override void RoomButtonPressed(GameObject selectedGameObject)
    {
        string blinkName = selectedGameObject.GetComponentInChildren<Text>().text;
        if (SQLPlayerInfo.SQLManager.Instance.UpdateBlinkInfo(ViewRoomController.CurrentSelectedPlayerName, blinkName))
        {
            GameObject.FindObjectOfType<CurrentBlinkSelected>().SetBlinkSelected(blinkName);
        }
        ViewRoomController.ShowBlinkSelectionScreen(false);
    }
}
