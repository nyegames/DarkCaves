﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentBlinkSelected : MonoBehaviour
{
    private RoomAndPassiveUIController _ViewRoomController;
    private Text VisualActiveBlink;

    public string BlinkSelected;

    private void OnEnable()
    {
        VisualActiveBlink = GetComponent<Text>();
        _ViewRoomController = FindObjectOfType<RoomAndPassiveUIController>();

        SetBlinkSelected(SQLPlayerInfo.SQLManager.Instance.GetPlayerActiveBlink(_ViewRoomController.CurrentSelectedPlayerName));
    }

    // Use this for initialization
    void Start()
    {
        VisualActiveBlink = GetComponent<Text>();
        _ViewRoomController = FindObjectOfType<RoomAndPassiveUIController>();

        SetBlinkSelected(SQLPlayerInfo.SQLManager.Instance.GetPlayerActiveBlink(_ViewRoomController.CurrentSelectedPlayerName));
    }

    public void SetBlinkSelected(string blinkName)
    {
        if (string.IsNullOrEmpty(blinkName)) return;
        BlinkSelected = blinkName;
        VisualActiveBlink.text = "[Active Blink] = " + blinkName;
    }
}
