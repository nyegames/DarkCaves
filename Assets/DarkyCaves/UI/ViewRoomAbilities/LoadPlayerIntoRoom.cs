﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LoadPlayerIntoRoom : Photon.PunBehaviour
{
    public override void OnJoinedRoom()
    {
        //Disable the message queue before loading into the scene, so that you dont miss any messages that happen during the loading time (like other players who have been created)
        PhotonNetwork.isMessageQueueRunning = false;

        //Create the ConnectionController to handle your connection to the game
        GameObject ConnectionController = Instantiate((GameObject)Resources.Load("ConnectionManager"));
        ConnectionController.name = "ConnectionController";
        ConnectionController.GetComponent<PhotonView>().viewID = 1;

        //Ask the current room you joined, what Scene you have to load to enter the game
        string sceneToLoad = (string)PhotonNetwork.room.CustomProperties["SceneName"];
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneToLoad);

        //Load that scene and wait for it to be changed into
        UnityEngine.SceneManagement.SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
    }

    private void SceneManager_activeSceneChanged(UnityEngine.SceneManagement.Scene arg0, UnityEngine.SceneManagement.Scene arg1)
    {
        UnityEngine.SceneManagement.SceneManager.activeSceneChanged -= SceneManager_activeSceneChanged;

        //Now that you have entered the scene required for this Room, enable the message queue to listen out for any PhotonNetwork messages you could have missed
        PhotonNetwork.isMessageQueueRunning = true;
        PlayerController.Instance.photonView.RPC("RequestPlayer", PhotonTargets.AllBufferedViaServer, PhotonNetwork.player.ID);
    }
}
