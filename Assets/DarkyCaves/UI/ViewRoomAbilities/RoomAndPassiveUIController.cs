﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RoomAndPassiveUIController : MonoBehaviour
{
    public enum ControllerState
    {
        PRE_GAME,
        IN_GAME
    }

    public ControllerState CurrentControllerState = ControllerState.PRE_GAME;

    /// <summary>The button that allows you to open the CreateNewPlayerForm</summary>
    public GameObject CreateNewPlayerButton;

    /// <summary>The Form used to create the new player after the user has entered the name</summary>
    public GameObject CreateNewPlayerForm;

    /// <summary>The text that shows you the currently selected PlayerName</summary>
    public GameObject SelectedPlayerText;
    private string _CurrentSelectedPlayerName;
    /// <summary>The current name of the selected player</summary>
    public string CurrentSelectedPlayerName
    {
        get { return _CurrentSelectedPlayerName; }
        set
        {
            _CurrentSelectedPlayerName = value;
            PlayerPrefs.SetString(PlayerPrefConstants.CurrentPlayerName, _CurrentSelectedPlayerName);
        }
    }

    /// <summary>The scrolling list of all players names in the database</summary>
    public GameObject ScrollablePlayerNames;

    /// <summary>Checks if the current amount of players in the database is less than 5, if it is then you can create more
    /// Otherwise you cant</summary>
    public void CheckCreateNewPlayerButtonShowable()
    {
        CreateNewPlayerButton.SetActive(SQLPlayerInfo.SQLManager.Instance.GetAllPlayers().Count() < 5);
    }


    /// <summary>The scrollable room browser, showing all of the currently active rooms</summary>
    public GameObject ScrollingRoomBrowser;
    /// <summary>The game object which will allow you to enter a name of a room, create and join it</summary>
    public GameObject CreateRoomController;

    /// <summary>Updates the visual for showing the selected player, with the currently selected player
    /// NOT USED TO STORE THE CURRENT SELECTED PLAYER, ONLY IN THE CREATE NEW PLAYER FORM</summary>
    /// <param name="selectedPlayer"></param>
    public void SelectedPlayerName(string selectedPlayer)
    {
        SelectedPlayerText.GetComponent<Text>().text = selectedPlayer;
    }

    /// <summary>Always visible, shows any error messages the user will need to see during this screen</summary>
    public GameObject ErrorMessageText;


    //ALL BUTTONS THAT OPEN PLAYER CHANGING INFORMATION, REQUIRE A PLAYER TO BE SELECTED FIRST
    //THEY WILL ALL BE A CHILD OF THIS GAMEOBJECT
    public GameObject PlayerInformationOpenButtons;

    /*******       PASIVE SELECTION SCREEN *************/
    //Part of this screens behaviour has been put into "PassiveSelectionScreenController" which is attached to the "PassiveSelectionScreen" GameObject

    /// <summary>The button you press to bring up the passive selection screen, can only be active if there is a current player selected</summary>
    public GameObject PassiveSelectionButton;

    /// <summary>Contains the screen that allows you to select and assign passives to each passive slot</summary>
    public GameObject PassiveSelectionScreen;

    /// <summary>The list that will be populated by the currently available passives the PlayerName that is selected, has available to them
    /// This will be in a new table in the PlayerInformation database, UnlockedPassives</summary>
    public GameObject PassiveEquipScreen;


    /*********** BLINK SELECTION SCREEN **************/
    //All gameobjects on screen which will let the player select their Blink behaviour

    public GameObject BlinkScreenOpenButton;

    public GameObject BlinkSelectionScreen;

    /*********** ABILITY SELECTION SCREEN ******************/
    //All gameobjects to do with the selection of the players Hand Abilties, Power Fists

    public GameObject HandAbilitiesScreenOpenButton;
    public GameObject FistingPowersSelectionScreen;


    /*********** STAT SELECTION SCREEN ********************/
    //All game objects to do with selecting the players stats, Health, Shield and Damage

    public GameObject StatSelectionScreenOpenButton;
    public GameObject StatSelectionScreen;

    /// <summary>True if all the game objects have already been found and stored</summary>
    private bool _Stored;

    /// <summary>True if the UI is showing</summary>
    private bool _ShowingUI = true;

    // Use this for initialization
    void OnEnable()
    {
        EnableAllChildren(false);

        Canvas canvas = GetComponent<Canvas>();
        canvas.worldCamera = Camera.main;

        bool wasFirstTime = !_Stored;
        if (!_Stored)
        {
            _Stored = true;

            _ShowingUI = CurrentControllerState == ControllerState.PRE_GAME;

            CreateNewPlayerButton = gameObject.GetChildWithName("CreatePlayerButton");
            SelectedPlayerText = gameObject.GetChildWithName("PlayerSelected");
            ScrollablePlayerNames = gameObject.GetChildWithName("ScrollablePlayerNames");

            CreateNewPlayerForm = gameObject.GetChildWithName("CreateNewPlayerForm");
            ScrollingRoomBrowser = gameObject.GetChildWithName("ScrollRoomBrowser");
            CreateRoomController = gameObject.GetChildWithName("CreateRoomController");

            ErrorMessageText = gameObject.GetChildWithName("ErrorMessage");

            PlayerInformationOpenButtons = gameObject.GetChildWithName("PlayerInformationOpenButtons");

            // ------------- PASSIVE SELECTION SCREEN ------------------- //

            PassiveSelectionButton = PlayerInformationOpenButtons.gameObject.GetChildWithName("PassiveSelectionScreenOpenButton");

            PassiveSelectionScreen = gameObject.GetChildWithName("PassiveSelectionScreen");

            PassiveEquipScreen = gameObject.GetChildWithName("PassiveEquipScreen");

            // --------------- BLINK SELECTION SCREEN ----------------------//

            BlinkScreenOpenButton = PlayerInformationOpenButtons.gameObject.GetChildWithName("BlinkSelectionScreenOpenButton");
            BlinkSelectionScreen = gameObject.GetChildWithName("BlinkSelectionScreen");

            // ------------------ HAND ABILITY SELECTION SCREEN ------------------- ///

            HandAbilitiesScreenOpenButton = PlayerInformationOpenButtons.gameObject.GetChildWithName("HandAbilitiesScreenOpenButton");
            FistingPowersSelectionScreen = gameObject.GetChildWithName("FistingPowersSelectionScreen");

            // ------------------- STAT SELECTION SCREEN ---------------------------//

            StatSelectionScreenOpenButton = gameObject.GetChildWithName("CharacterStatsScreenOpenButton");
            StatSelectionScreen = gameObject.GetChildWithName("StatSelectionScreen");
        }

        EnableAllChildren(true);

        PassiveSelectionScreen.SetActive(false);
        PassiveEquipScreen.SetActive(false);
        BlinkSelectionScreen.SetActive(false);
        FistingPowersSelectionScreen.SetActive(false);
        StatSelectionScreen.SetActive(false);

        //If this is your first launch, then launch into the CreateNewPlayerForm screen
        ShowCreateNewPlayerFormScreen(SQLPlayerInfo.SQLManager.Instance.GetAllPlayers().Count() == 0);
        BackToMainMenu();

        if (wasFirstTime && CurrentControllerState == ControllerState.IN_GAME)
        {
            EnableAllChildren(false);
            return;
        }

        string currentPlayerName = PlayerPrefs.GetString(PlayerPrefConstants.CurrentPlayerName);
        if (string.IsNullOrEmpty(currentPlayerName) || SQLPlayerInfo.SQLManager.Instance.GetAllPlayers().Count() == 0) return;

        SelectedPlayerName(currentPlayerName);
        PlayerSelected(currentPlayerName);
    }

    /// <summary>When returning from a selection screen, back to show the default screen for this UI Object</summary>
    private void BackToMainMenu()
    {
        if (CurrentControllerState == ControllerState.PRE_GAME)
        {
            CreateNewPlayerButton.SetActive(SQLPlayerInfo.SQLManager.Instance.GetAllPlayers().Count() < SQLPlayerInfo.SQLManager.PlayerPassiveMax);

            CreateRoomController.SetActive(!string.IsNullOrEmpty(CurrentSelectedPlayerName));
        }
        else if (CurrentControllerState == ControllerState.IN_GAME)
        {
            CreateNewPlayerButton.SetActive(false);
            CreateNewPlayerForm.SetActive(false);
            ScrollablePlayerNames.SetActive(false);
            ScrollingRoomBrowser.SetActive(false);
            CreateRoomController.SetActive(false);
        }
    }

    public void PlayerSelected(string playerName)
    {
        CurrentSelectedPlayerName = playerName;
        PhotonNetwork.playerName = playerName;

        SelectedPlayerText.GetComponent<Text>().text = playerName;

        CreateRoomController.SetActive(true && CurrentControllerState == ControllerState.PRE_GAME);
        ScrollingRoomBrowser.SetActive(true && CurrentControllerState == ControllerState.PRE_GAME);

        PlayerInformationOpenButtons.SetActive(true);
    }

    /************************ 
     * -------------------- SELECTING A SCREEN ------------------ *
     * *************************************************************/


    private void OpenAnySelectionScreen(bool open)
    {
        bool playerSelected = !string.IsNullOrEmpty(SelectedPlayerText.GetComponent<Text>().text);

        ScrollingRoomBrowser.SetActive(!open && playerSelected);

        CreateRoomController.SetActive(!open);
        ScrollablePlayerNames.SetActive(!open);

        bool maxPlayersReached = SQLPlayerInfo.SQLManager.Instance.GetAllPlayers().Count() >= SQLPlayerInfo.SQLManager.PlayerPassiveMax;
        CreateNewPlayerButton.SetActive(!open && !maxPlayersReached);
    }

    /// <summary>Open the screen for the user to create a new player,
    /// You only wasnt to be able to see this screen, nothing else</summary>
    public void ShowCreateNewPlayerFormScreen(bool open)
    {
        CreateNewPlayerForm.SetActive(open);
        CreateNewPlayerButton.SetActive(!open);

        SelectedPlayerText.SetActive(!open);
        CreateRoomController.SetActive(!open);

        bool playerSelected = !string.IsNullOrEmpty(SelectedPlayerText.GetComponent<Text>().text);

        ScrollingRoomBrowser.SetActive(!open && playerSelected);

        PlayerInformationOpenButtons.SetActive(!open && playerSelected);

        if (!open) BackToMainMenu();
    }

    /// <summary>
    /// Show the passive selection screen, not equipping them just the 6 passive boxes you can see what you have equip
    /// </summary>
    /// <param name="open"></param>
    public void ShowPassiveSelectionScreen(bool open)
    {
        OpenAnySelectionScreen(open);

        PassiveSelectionScreen.SetActive(open);

        PlayerInformationOpenButtons.SetActive(!open);

        if (!open) BackToMainMenu();
    }

    /// <summary>Show the BlinkSelection screen, to select the blink you want equiped</summary>
    /// <param name="open"></param>
    public void ShowBlinkSelectionScreen(bool open)
    {
        OpenAnySelectionScreen(open);

        BlinkSelectionScreen.SetActive(open);

        PlayerInformationOpenButtons.SetActive(!open);

        if (!open) BackToMainMenu();
    }

    /// <summary>Show the screen of the current hand abilities you have attached to each hand</summary>
    /// <param name="open"></param>
    public void ShowHandAbilitiesSelectionScreen(bool open)
    {
        OpenAnySelectionScreen(open);

        FistingPowersSelectionScreen.SetActive(open);

        PlayerInformationOpenButtons.SetActive(!open);

        if (!open) BackToMainMenu();
    }

    /// <summary>Show the selection screen for the players current stats, stored in the PlayerPrefs</summary>
    /// <param name="open"></param>
    public void ShowStatSelectionScreen(bool open)
    {
        OpenAnySelectionScreen(open);

        StatSelectionScreen.SetActive(open);

        PlayerInformationOpenButtons.SetActive(!open);

        if (!open) BackToMainMenu();
    }

    /************************************************ END OF SELECTION SCREENS *****************************************/

    public void EnterGame()
    {
        CurrentControllerState = ControllerState.IN_GAME;

        EnableAllChildren(false);
    }

    public void EnableAllChildren(bool enable = true)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(enable);
        }
    }

    private void Update()
    {
        if (CurrentControllerState == ControllerState.IN_GAME && Input.GetKeyDown(KeyCode.Escape))
        {
            _ShowingUI = !_ShowingUI;
            if (_ShowingUI) OnEnable();
            else EnableAllChildren(false);

            if (_ShowingUI)
            {
                PlayerController.Instance.GetPlayerObject(PhotonNetwork.player.ID).GetComponent<UpdateManger>().PauseUpdate = true;
                return;
            }
            PlayerController.Instance.photonView.RPC("UpdatePlayerInfo", PhotonTargets.AllBuffered, PhotonNetwork.player.ID);
        }
    }
}
