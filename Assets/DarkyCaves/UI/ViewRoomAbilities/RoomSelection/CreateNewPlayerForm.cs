﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateNewPlayerForm : MonoBehaviour
{
    public PlayerNamesToSelectFrom PlayerNamesToSelect;
    private string PlayerName;

    public void SetPlayerName(object newname)
    {
        PlayerName = (string)newname;
    }

    public void InsertPlayer()
    {
        Text txt = GameObject.Find("ErrorMessage").GetComponent<Text>();
        if (string.IsNullOrEmpty(PlayerName) || PlayerName.Length < 2 || PlayerName.Length > 10)
        {
            txt.text = "A name has to have at least 2 characters and not more than 10";
            return;
        }
        txt.text = string.Format("Player '{0}' created!", PlayerName);

        SQLPlayerInfo.SQLManager.Instance.CreateNewPlayer(PlayerName);
        if (PlayerNamesToSelect == null) PlayerNamesToSelect = FindObjectOfType<PlayerNamesToSelectFrom>();
        PlayerNamesToSelect.UpdatePlayerNames();
    }
}
