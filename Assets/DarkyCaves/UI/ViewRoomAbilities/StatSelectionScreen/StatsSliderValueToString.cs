﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsSliderValueToString : MonoBehaviour
{
    private Text _SliderText;

    /// <summary>Update the info text at the end of the slider demonstrating its value given in the slider</summary>
    /// <param name="newValue"></param>
    public void SliderValueToPercentageString(float newValue)
    {
        if (_SliderText == null) _SliderText = GetComponentInChildren<Text>();

        _SliderText.text = newValue.ToString("N0");
    }
}
