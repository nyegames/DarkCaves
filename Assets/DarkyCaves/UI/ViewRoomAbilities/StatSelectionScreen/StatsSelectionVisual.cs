﻿using Glide;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StatsSelectionVisual : MonoBehaviour
{
    public GameObject SliderPrefab;
    private Tweener _Tweener = new Tweener();

    public Dictionary<StatSelectionScreen.PointType, GameObject> StatSliders = new Dictionary<StatSelectionScreen.PointType, GameObject>();

    public float HeightGap = 160;


    private StatSelectionScreen _StatSelectionScreen;
    private StatStrengthsToText _StatStrengthToText;

    // Update is called once per frame
    void Update()
    {
        _Tweener.Update(Time.deltaTime);
    }

    /// <summary>Create a StatSlider prefab, using the PointType to provide the base information</summary>
    /// <param name="pointyType"></param>
    /// <param name="TotalAvailablePoints"></param>
    public void CreateSlider(StatSelectionScreen.PointType pointyType, float TotalAvailablePoints)
    {
        GameObject slider = Instantiate(SliderPrefab);
        slider.name = pointyType.Name;
        RectTransform rectT = slider.GetComponent<RectTransform>();
        rectT.transform.parent = transform;
        rectT.localScale = Vector2.one;
        rectT.anchoredPosition3D = new Vector3(100, -(StatSliders.Count * HeightGap) - 100f, 0f);
        rectT.sizeDelta = new Vector2(TotalAvailablePoints, rectT.sizeDelta.y);
        slider.GetComponentInChildren<Text>().text = pointyType.Name;
        StatSliders.Add(pointyType, slider);

        slider.GetChildWithName("BarVisual").GetComponent<RectTransform>().sizeDelta = new Vector2(pointyType.Current, rectT.sizeDelta.y);

        Slider sliderComp = slider.GetComponent<Slider>();
        sliderComp.maxValue = TotalAvailablePoints;
        sliderComp.value = pointyType.Current;
    }

    /// <summary>Search for the stored PointType using the provided paramaters, then update the visual information in the UI about this stat</summary>
    /// <param name="typeName"></param>
    /// <param name="newValue"></param>
    public void UpdateStatValue(string typeName, int newValue)
    {
        //Grab the PointType using the name as reference
        var pointyType = StatSliders.Keys.FirstOrDefault(s => s.Name.Equals(typeName));
        if (pointyType == null) return;

        //Calculate the amount the stat value has changed by, this value has to be reversed to the other components
        int changeAmount = newValue - pointyType.Current;

        //If nothing has changed, who cares?
        if (changeAmount == 0) return;

        //Update the PointType current value with the new one selected
        pointyType.Current = newValue;

        if (_StatSelectionScreen == null) _StatSelectionScreen = FindObjectOfType<StatSelectionScreen>();

        //Change the visual rect transform to be the size of the new value selected
        GameObject slider = StatSliders[pointyType];
        RectTransform rectT = slider.GetComponent<RectTransform>();
        slider.GetChildWithName("BarVisual").GetComponent<RectTransform>().sizeDelta = new Vector2(pointyType.Current, rectT.sizeDelta.y);

        //Pause any updates to value changes, this would cause recursive change events otherwise
        StatSelectionScreen.PauseValueUpdates = true;

        //Cause the current slider to be updated, just incase
        slider.GetComponent<Slider>().value = pointyType.Current;

        //Create a list, to be populated by all the information that needs to be updated
        List<StatSelectionScreen.PointType> updateThese = new List<StatSelectionScreen.PointType>();

        var tempStatSliders = StatSliders.ToList();
        tempStatSliders.OrderBy(s => s.Key.Current);

        foreach (var pointT in tempStatSliders)
        {
            if (pointT.Key.Name == typeName) continue;
            //If you are trying to remove something below its minimum
            if (changeAmount > 0 && pointT.Key.Current <= 0) continue;
            updateThese.Add(pointT.Key);
        }

        if (updateThese.Count() > 0)
        {
            int newMaxVal = (changeAmount * -1) / updateThese.Count();
            int small = 0;
            int large = 0;
            int extra = 0;
            if (updateThese.Count() == 2 && changeAmount % 2 != 0)
            {
                small = newMaxVal;
                large = newMaxVal + (changeAmount * -1) % 2;
                newMaxVal = 0;
            }

            foreach (StatSelectionScreen.PointType sliderName in updateThese)
            {
                if (newMaxVal == 0) newMaxVal += small != 0 ? small : large != 0 ? large : 0;
                newMaxVal += extra;

                //Removing from others
                if (changeAmount > 0)
                {
                    extra = sliderName.Current + newMaxVal < 0 ? sliderName.Current + newMaxVal : 0;
                }
                else
                {
                    extra = sliderName.Current + newMaxVal > _StatSelectionScreen.TotalPoints ? _StatSelectionScreen.TotalPoints - sliderName.Current + newMaxVal : 0;
                }
                extra = 0;

                RectTransform visSlideRectT = StatSliders[sliderName].GetChildWithName("BarVisual").GetComponent<RectTransform>();
                visSlideRectT.sizeDelta += new Vector2(newMaxVal, 0f);

                var sliderComp = StatSliders[sliderName].GetComponent<Slider>();
                sliderComp.value += newMaxVal;
                sliderName.Current += newMaxVal;

                if (small != 0)
                {
                    small = 0;
                    newMaxVal = 0;
                }
                else if (large != 0)
                {
                    large = 0;
                    newMaxVal = 0;
                }
            }
        }

        LinkVisualsToPointTypeData();

        StatSelectionScreen.PauseValueUpdates = false;
    }

    public void LinkVisualsToPointTypeData()
    {
        foreach (var pt in StatSliders.Keys)
        {
            StatSliders[pt].GetComponent<Slider>().value = pt.Current;
            RectTransform visSlideRectT = StatSliders[pt].GetChildWithName("BarVisual").GetComponent<RectTransform>();
            visSlideRectT.sizeDelta = new Vector2(pt.Current, visSlideRectT.sizeDelta.y);
        }

        //Update the text showing the base stats that have been calculated
        if (_StatStrengthToText == null) _StatStrengthToText = FindObjectOfType<StatStrengthsToText>();
        _StatStrengthToText.UpdateStatStrengthInfo();
    }

    public void DestroySliders()
    {
        foreach (var slide in StatSliders)
        {
            Destroy(slide.Value);
        }
        StatSliders.Clear();
    }
}
