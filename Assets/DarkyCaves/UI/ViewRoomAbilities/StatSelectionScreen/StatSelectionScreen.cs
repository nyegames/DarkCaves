﻿using DarkyEvents;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StatSelectionScreen : MonoBehaviour
{
    public static bool PauseValueUpdates;
    public static bool SlidersCreated;

    private RoomAndPassiveUIController _RoomPassiveController;
    private Text _ErrorText;

    private StatsSelectionVisual _StatsSelectionVisual;

    private Text _StatSelectionSaveExitButtonText;

    /// <summary>The total amount of points available to the player to send across Health,Shield and Damage</summary>
    public int TotalPoints = 1000;

    /// <summary>The current amount of points left to assign to Health, Shield or Damage</summary>
    public int AvailablePoints;

    [System.Serializable]
    public class PointType
    {
        /// <summary>Human readable name for this PointType</summary>
        public string Name;
        /// <summary>The minimum required amount for this PointType, you cannot have less than this amount</summary>
        public int MinRequired;
        /// <summary>Current amount of Points assigned to this PointType</summary>
        public int Current;
    }

    public List<PointType> PointTypes;

    /// <summary>True if the current selection of Points applies between all PointTypes is a valid setup</summary>
    public bool SelectionAvailable;

    // Use this for initialization
    void Start()
    {
        _RoomPassiveController = FindObjectOfType<RoomAndPassiveUIController>();
        _ErrorText = _RoomPassiveController.ErrorMessageText.GetComponent<Text>();
        _StatSelectionSaveExitButtonText = gameObject.GetChildWithName("StatSelectionSaveExitButton").GetComponentInChildren<Text>();
    }

    private void OnEnable()
    {
        if (SlidersCreated) return;
        PointTypes.Clear();
        if (_StatsSelectionVisual != null) _StatsSelectionVisual.DestroySliders();
        CreateSliders();
    }

    public void CreateSliders()
    {
        SlidersCreated = true;

        AvailablePoints = TotalPoints;

        if (PointTypes.Where(a => a.Name.Equals("Health")).Count() == 0) { PointTypes.Add(new PointType() { Name = "Health", MinRequired = 10 }); }
        if (PointTypes.Where(a => a.Name.Equals("Shield")).Count() == 0) { PointTypes.Add(new PointType() { Name = "Shield" }); }
        if (PointTypes.Where(a => a.Name.Equals("Damage")).Count() == 0) { PointTypes.Add(new PointType() { Name = "Damage", MinRequired = 20 }); }

        _StatsSelectionVisual = GetComponent<StatsSelectionVisual>();

        foreach (PointType pt in PointTypes)
        {
            pt.Current = PlayerPrefs.GetInt(pt.Name);
            _StatsSelectionVisual.CreateSlider(pt, TotalPoints);
        }
    }

    // Update is called once per frame
    void Update()
    {
        SelectionAvailable = false;
        _StatSelectionSaveExitButtonText.text = "Exit";
        AvailablePoints = PointTypes.Sum(s => s.Current);
        foreach (PointType pt in PointTypes)
        {
            if ((int)(pt.Current + 0.5f) < pt.MinRequired)
            {
                _ErrorText.text = string.Format("{0} requires at least {1} points invested", pt.Name, pt.MinRequired);
                return;
            }
        }
        int clampAvailable = (int)(AvailablePoints + 0.5f);
        if (clampAvailable > TotalPoints || clampAvailable < TotalPoints)
        {
            _ErrorText.text = "Something went wrong, reset your stats and try again";
            return;
        }

        SelectionAvailable = clampAvailable == (int)TotalPoints;
        _ErrorText.text = "Exit now to save your Stats distribution";
        _StatSelectionSaveExitButtonText.text = "Save and Exit";
    }

    public void UpdateStatValue(string typeName, int newValue)
    {
        var pointyType = PointTypes.FirstOrDefault(s => s.Name.Equals(typeName));
        if (pointyType == null) return;
        _StatsSelectionVisual.UpdateStatValue(typeName, newValue);
        pointyType.Current = newValue;
    }

    public void ResetSliders()
    {
        PauseValueUpdates = true;
        int[] points =
        {
            334,333,333
        };
        for (int i = 0; i < PointTypes.Count; i++)
        {
            PointTypes[i].Current = points[i];
        }
        _StatsSelectionVisual.LinkVisualsToPointTypeData();
        PauseValueUpdates = false;
    }

    private void OnDisable()
    {
        if (_StatsSelectionVisual != null) _StatsSelectionVisual.DestroySliders();
        SlidersCreated = false;
        if (!SelectionAvailable) return;

        foreach (PointType pt in PointTypes)
        {
            PlayerPrefs.SetInt(pt.Name, pt.Current);
        }
        PointTypes.Clear();
    }
}
