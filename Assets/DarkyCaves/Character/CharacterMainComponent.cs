﻿using DarkyEvents;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>The CharacterTransform class is the hub for all the Characters behaviour
/// Anything you want to know about the Character you should be able to get through the CharacterTransform class
/// Will contain router methods, which will query its own objects but provide a direct getter through this class for</summary>
[RequireComponent(typeof(Rigidbody2D), typeof(CharacterStats))]
[RequireComponent(typeof(CharacterBuffController)), RequireComponent(typeof(UpdateManger))]
public class CharacterMainComponent : Photon.MonoBehaviour
{
    private UpdateManger _UpdateManager;

    private Rigidbody2D _CharacterRigidBody;
    public CharacterStats CharacterStats;

    public Vector3 FacingVector;
    /// <summary>This is the amount of degrees to change the angle of facing when moving</summary>
    public float DegreeFacingVectorOffset = -90f;

    public CharacterController.DirectionalState CharacterDirectionalState = CharacterController.DirectionalState.DOWN;

    public string ProjectilePrefabName = "Default_Projectile";
    public Transform ProjectileAnchor;

    public GameObject ThrowableGameObject;
    public ThrowableObject ThrowableObject;

    public CharacterBuffController CharacterBuffController;

    public CharacterAnimationController CharacterAnimationController;

    /// <summary>The base amount of damage this character inflicts,
    /// all damage modifiers will use this value to calculate their own damage</summary>
    public DamageType BaseDamage
    {
        get
        {
            return new DamageType(DamageType.ElementType.NORMAL, CharacterStats.BaseDamage);
        }
    }

    void Awake()
    {
        CharacterBuffController = GetComponent<CharacterBuffController>();
        _CharacterRigidBody = GetComponent<Rigidbody2D>();
        CharacterStats = GetComponent<CharacterStats>();
        CharacterAnimationController = GetComponentInChildren<CharacterAnimationController>();

        ProjectileAnchor = _CharacterRigidBody.transform.FindChild("Projectile_Origin");
        ThrowableGameObject = Instantiate(Resources.Load(ProjectilePrefabName)) as GameObject;
        SmoothFollow sf = ThrowableGameObject.GetComponentInChildren<SmoothFollow>();
        sf.target = ProjectileAnchor;
        ThrowableObject = ThrowableGameObject.GetComponentInChildren<ThrowableObject>();
        ThrowableObject.RegisterHandOwner(CharacterStats, this);
    }

    [PunRPC]
    public void UpdateDirectionalState(CharacterController.DirectionalState newDirection)
    {
        CharacterDirectionalState = newDirection;
        CharacterAnimationController.UpdateDirectionalState(newDirection);
    }

    [PunRPC]
    public void UpdateForceState(CharacterController.ForceStates newForce)
    {
        CharacterAnimationController.UpdateForceState(newForce);
    }

    private void Start()
    {
        CharacterBuffController.LoadPassives();
    }

    /// <summary>Moves the Character along a certain Vector</summary>
    /// <param name="nMovementVector"></param>
    public void MoveCharacterAlongVector(Vector3 nMovementVector)
    {
        if (nMovementVector == Vector3.zero) return;

        Vector3 newMove = new Vector3(Mathf.Clamp(nMovementVector.x, -1, 1), Mathf.Clamp(nMovementVector.y, -1, 1), 0);
        newMove.Normalize();
        newMove *= CharacterStats.CalculateMovementSpeed();
        _CharacterRigidBody.AddForce(newMove, ForceMode2D.Force);
        RotateToFaceVector(newMove);
    }

    /// <summary>Move the Character towards a given target Position</summary>
    /// <param name="target"></param>
    public void MoveCharacterTowardsPosition(Vector3 targetPosition)
    {
        MoveCharacterAlongVector(targetPosition - transform.position);
    }

    /// <summary>Instantly rotate to face the movement vector you are travelling in</summary>
    /// <param name="moveVector"></param>
    protected void RotateToFaceVector(Vector3 moveVector)
    {
        FacingVector = moveVector;
        bool left = moveVector.x < 0;
        //float angle = (Mathf.Atan2(moveVector.y, moveVector.x) * Mathf.Rad2Deg) + DegreeFacingVectorOffset;
        //transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        //transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
        transform.localScale = left ? new Vector3(-1, 1, 1) : new Vector3(1, 1, 1);
    }

    /// <summary>Ask the master client to check his instance of this object to see if it is in a position to Throw the hand requested.
    /// If it is, then forward on the RPC request to throw the hand to all these instances</summary>
    /// <param name="index"></param>
    /// <param name="throwVector"></param>
    /// <param name="info"></param>
    [PunRPC]
    public void RequestThrowObject(Vector3 throwVector, PhotonMessageInfo info)
    {
        if (!ThrowableObject.CanThrowHand()) return;
        photonView.RPC("ThrowObject", PhotonTargets.All, new object[] { ThrowableObject.transform.position, throwVector });
    }

    /// <summary>Throw the hand at the index down the throwVector</summary>
    /// <param name="index"></param>
    /// <param name="throwVector"></param>
    /// <param name="info"></param>
    [PunRPC]
    public void ThrowObject(Vector3 startingHandPosition, Vector3 throwVector, PhotonMessageInfo info)
    {
        ThrowableObject.transform.position = startingHandPosition;
        ThrowableObject.ThrowObject(throwVector);
    }

    /// <summary>Request the master that the hand be returned, the master will check to see if the hand on its version of the game is allowed to return yet</summary>
    /// <param name="index"></param>
    [PunRPC]
    public void RequestStartReturning(int index)
    {
        if (!ThrowableObject.CanStartReturning()) return;
        photonView.RPC("StartReturning", PhotonTargets.All, new object[] { false, (Vector2)ThrowableObject.transform.position });
    }

    /// <summary>Calculates the path the hand should take to intersect along the ThrowVector from the Character,
    /// at a distance (Current throw vector distance) from the Character itself</summary>
    /// <param name="handIndex"></param>
    /// <param name="throwVector"></param>
    /// <returns></returns>
    internal Vector3 CalculateHandThrowVector(Vector3 throwVector)
    {
        Vector3 throwToPoint = transform.position + throwVector;
        Vector3 handThrowVector = throwToPoint - ThrowableObject.transform.position;
        return handThrowVector;
    }

    /// <summary>Request the master that the hand be returned, the master will check to see if the hand on its version of the game is allowed to return yet</summary>
    /// <param name="index"></param>
    [PunRPC]
    public void RequestUserEarlyStartReturning()
    {
        if (!ThrowableObject.CanStartReturning()) return;
        photonView.RPC("StartReturning", PhotonTargets.All, new object[] { true, (Vector2)ThrowableObject.transform.position });
    }

    /// <summary>Start returning the hand
    /// If forcedReturn is true, then it will ignore the time limit before the hand is allowed to be returned
    /// This is used for when the hand has hit an enemy, and not reached its max time allowed out before being naturally returned</summary>
    /// <param name="index"></param>
    /// <param name="userRequestReturn"></param>
    [PunRPC]
    public void StartReturning(bool userRequestReturn, Vector2 pointReturnedFrom)
    {
        ThrowableObject.StartReturning(userRequestReturn, pointReturnedFrom);
    }


    /// <summary>When the master client detects that there has been a collision on a hand, the hand will then call this.
    /// This is always called from the master client's Hand, so doesn't require to be a RPC.
    /// It will trigger all of the attack actions, and start returning the hand. Ignoring time limits before the hand is allowed to be recalled by the player</summary>
    /// <param name="index"></param>
    public void ThrowObjectCollided(int attackablePhotonID, Vector2 collisionPoint)
    {
        photonView.RPC("TriggerAttackActions", PhotonTargets.All, new object[] { attackablePhotonID, collisionPoint });

        //If the hand isn't currently OUT then it will already be returningdw
        if (ThrowableObject.CurrentThrownState != ThrowableObject.ThrowableState.OUT) return;
        photonView.RPC("StartReturning", PhotonTargets.All, new object[] { false, (Vector2)ThrowableObject.transform.position });
    }

    [PunRPC]
    public void TriggerReturnedActions()
    {
        ThrowableObject.TriggerReturnedActions(ThrowableObject.transform.position);
    }

    [PunRPC]
    public void TriggerAttackActions(int attackablePhotonID, Vector2 attackPoint, PhotonMessageInfo info)
    {
        IAttackable attackableHit = attackablePhotonID == -1 ? null :
            ((List<IAttackable>)PhotonView.Find(attackablePhotonID).gameObject.GetAllAttachedTypes(typeof(IAttackable), true)).FirstOrDefault();
        ThrowableObject.TriggerAttackActions(attackableHit, attackPoint);
    }


    /*----------------- BLINK  ---------------------- */

    /// <summary>Gets the first (and only) IOnBlink attached to this Character</summary>
    /// <returns></returns>
    protected IOnBlink GetActiveBlink()
    {
        //TODO Find a better way to select the Active IOnBlink from the Player
        List<BaseBlink> onBlink = (List<BaseBlink>)gameObject.GetAllAttachedTypes(typeof(BaseBlink), true);
        if (onBlink != null)
        {
            foreach (BaseBlink leBlink in onBlink)
            {
                return leBlink;
            }
        }
        return null;
    }

    /// <summary>Check to see if it is possible for a blink to be made</summary>
    /// <param name="distanceToBlink"></param>
    /// <param name="directionalVector"></param>
    /// <returns></returns>
    protected virtual bool CheckCanBlink(Vector3 directionalVector)
    {
        IOnBlink onBlink = GetActiveBlink();
        if (onBlink == null) return false;
        return onBlink.CheckCanBlink(directionalVector, this);
    }

    /// <summary>Request sent to the master client to see this character is allowed to blink</summary>
    /// <param name="blinkDistance"></param>
    /// <param name="blinkVector"></param>
    [PunRPC]
    public void RequestTriggerBlink(Vector3 blinkVector)
    {
        if (!CheckCanBlink(blinkVector)) return;
        photonView.RPC("TriggerBlink", PhotonTargets.All, new object[] { blinkVector });
    }

    /// <summary>Trigger a blink to move this character quickly in a direction</summary>
    /// <param name="blinkDistance"></param>
    /// <param name="blinkVector"></param>
    [PunRPC]
    public virtual void TriggerBlink(Vector3 blinkVector)
    {
        TriggerOnBlinkStart(transform.position);
        IOnBlink blink = GetActiveBlink();
        if (blink != null)
        {
            blink.TriggerBlink(blinkVector, this);
            DarkyEvents.EventManager.TriggerEvent(EventNames.BlinkTriggered, blink.BlinkCooldownSeconds, photonView.isMine);
        }
        TriggerOnBlinkFinished(transform.position);
    }

    /// <summary>For each attached IOnBlink script, call the OnBlinkStarted method on it</summary>
    /// <param name="blinkedFrom"></param>
    public void TriggerOnBlinkStart(Vector3 blinkedFrom)
    {
        IOnBlink onBlink = GetActiveBlink();
        if (onBlink == null) return;
        onBlink.OnBlinkStarted(this, blinkedFrom);
    }

    /// <summary>For each attached IOnBlink script, call the OnBlinkFinished method on it</summary>
    /// <param name="blinkedTo"></param>
    public void TriggerOnBlinkFinished(Vector3 blinkedTo)
    {
        IOnBlink onBlink = GetActiveBlink();
        if (onBlink == null) return;
        onBlink.OnBlinkFinished(this, blinkedTo);
    }

    /* --------------------------- PASSIVES -------------------------------- */

    /// <summary>For each IPassive item in the list, call this on it</summary>
    /// <param name="itemAction"></param>
    protected void ForEachAttachedIPassive(Action<IPassive> passiveAction)
    {
        IPassive[] passives = ((List<IPassive>)gameObject.GetAllAttachedTypes(typeof(IPassive), true)).ToArray();
        if (passives == null) return;
        foreach (IPassive passive in passives) { passiveAction.Invoke(passive); }
    }


    /* ------------------------ NETWORK SYNC ---------------------------- */


    private UpdateAction _UpdateNetworkObjectPositions;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        SerializeState(stream, info);
    }

    public void SerializeState(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //Main body position
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else if (stream.isReading)
        {
            int timesPerSecondUpdated = PhotonNetwork.sendRateOnSerialize;

            //If for whatever reason the update from the previous network data is still happening, kill the update and start again
            if (_UpdateNetworkObjectPositions != null) _UpdateNetworkObjectPositions.Cancel(true);
            _UpdateNetworkObjectPositions = null;

            Vector3 startPos = transform.position;
            Vector3 endPos = (Vector3)stream.ReceiveNext();

            Quaternion startRot = transform.rotation;
            Quaternion endRot = (Quaternion)stream.ReceiveNext();

            //Create the updating object which will perform a lerp over half the time it takes for the server to update.
            //This allows the clients to update all of the characters to their positions they were last update, before the next update is meant to arrive.
            _UpdateNetworkObjectPositions = gameObject.AddComponent<UpdateAction>();
            _UpdateNetworkObjectPositions.Play(t =>
            {
                transform.position = Vector3.Lerp(startPos, endPos, t);
                transform.rotation = Quaternion.Slerp(startRot, endRot, t);

            }, TimeSpan.FromSeconds((timesPerSecondUpdated / 2f) / 60f + PhotonNetwork.GetPing()), () =>
            {
                _UpdateNetworkObjectPositions = null;
                transform.position = endPos;
                transform.rotation = endRot;
            });
        }
    }

    private void OnDestroy()
    {
        if (ThrowableObject == null || ThrowableObject.gameObject == null) return;
        Destroy(ThrowableObject.gameObject);
    }
}
