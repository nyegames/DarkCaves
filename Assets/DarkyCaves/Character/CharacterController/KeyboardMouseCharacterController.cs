﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class KeyboardMouseCharacterController : CharacterController
{
    public KeyboardMouseCharacterController(CharacterControllerSwitcher.CharacterControllerTypes controllerType, CharacterMainComponent characterTransform, LineRenderer throwDirection) : base(controllerType, characterTransform, throwDirection)
    {
    }

    protected override Vector3 CalculateMovementVector()
    {
        Vector3 moveVec = Vector3.zero;
        //if (CrossPlatformInputManager.GetButton(CharacterInput.MoveForward)) moveVec.y = 1;
        //if (CrossPlatformInputManager.GetButton(CharacterInput.MoveBackwards)) moveVec.y = -1;

        if (CrossPlatformInputManager.GetButton(CharacterInput.MoveLeft)) moveVec.x = -1;
        if (CrossPlatformInputManager.GetButton(CharacterInput.MoveRight)) moveVec.x = 1;

        return CrossPlatformInputManager.GetButton(CharacterInput.LeftShift) ? new Vector3(CalculateThrowVector().x, 0, 0).normalized : moveVec.normalized;
    }

    protected override Vector3 CalculateThrowVector()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        mousePos.z = _CharacterTranform.transform.position.z;
        Vector3 throwVector = mousePos - _CharacterTranform.transform.position;
        float throwDistance = _CharacterTranform.CharacterStats.BaseHandThrowDistance;
        return throwVector.magnitude > throwDistance ? throwVector.normalized * throwDistance : throwVector;
    }

    public override bool CheckActivelyUsed()
    {
        return CrossPlatformInputManager.GetButton(CharacterInput.MoveForward) || CrossPlatformInputManager.GetButton(CharacterInput.MoveBackwards) ||
            CrossPlatformInputManager.GetButton(CharacterInput.MoveLeft) || CrossPlatformInputManager.GetButton(CharacterInput.MoveRight)
            || CrossPlatformInputManager.GetButton(CharacterInput.LeftShift);
    }

    protected override bool CheckToThrowLeftHand()
    {
        return CrossPlatformInputManager.GetButtonDown(CharacterInput.LeftMouse);
    }

    protected override bool CheckToThrowRightHand()
    {
        return CrossPlatformInputManager.GetButtonDown(CharacterInput.RightMouse);
    }

    protected override bool CheckWantToBlink()
    {
        return CrossPlatformInputManager.GetButtonDown(CharacterInput.Blink);
    }

    public override bool CheckWantToInteract()
    {
        return CrossPlatformInputManager.GetButtonDown(CharacterInput.Action);
    }

    public override bool CheckForJump()
    {
        return CrossPlatformInputManager.GetButton(CharacterInput.SpaceBar);
    }
}
