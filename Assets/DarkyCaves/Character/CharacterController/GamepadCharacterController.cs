﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class GamepadCharacterController : CharacterController
{
    public GamepadCharacterController(CharacterControllerSwitcher.CharacterControllerTypes controllerType, CharacterMainComponent characterTransform, LineRenderer throwDirection) : base(controllerType, characterTransform, throwDirection)
    {
    }

    protected override Vector3 CalculateMovementVector()
    {
        Vector3 movementVector = new Vector3()
        {
            x = CrossPlatformInputManager.GetAxis(CharacterInput.LeftStickX),
            y = 0//CrossPlatformInputManager.GetAxis(CharacterInput.LeftStickY)
        };
        return movementVector.normalized;
    }

    protected override Vector3 CalculateThrowVector()
    {
        Vector3 throwVector = new Vector3()
        {
            x = CrossPlatformInputManager.GetAxis(CharacterInput.RightStickX),
            y = CrossPlatformInputManager.GetAxis(CharacterInput.RightStickY)
        };
        float throwDistance = _CharacterTranform.CharacterStats.BaseHandThrowDistance;
        return throwVector * throwDistance;
    }

    public override bool CheckActivelyUsed()
    {
        return CrossPlatformInputManager.GetAxis(CharacterInput.LeftStickX) > CharacterInput.StickMinThreshold; ;
    }

    protected override bool CheckToThrowLeftHand()
    {
        return CrossPlatformInputManager.GetButtonDown(CharacterInput.ThrowLeft);
    }

    protected override bool CheckToThrowRightHand()
    {
        return CrossPlatformInputManager.GetButtonDown(CharacterInput.ThrowRight);
    }

    protected override bool CheckWantToBlink()
    {
        return CrossPlatformInputManager.GetButtonDown(CharacterInput.Blink);
    }

    public override bool CheckWantToInteract()
    {
        return CrossPlatformInputManager.GetButtonDown(CharacterInput.Action);
    }

    public override bool CheckForJump()
    {
        return CrossPlatformInputManager.GetButton(CharacterInput.SpaceBar);
    }
}
