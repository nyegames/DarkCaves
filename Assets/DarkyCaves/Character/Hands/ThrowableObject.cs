﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>The base class used for a ThrowAbleHand</summary>
[RequireComponent(typeof(CircleCollider2D), typeof(SmoothFollow))]
public class ThrowableObject : MonoBehaviour
{
    /// <summary>The ThrowableHands hitbox area</summary>
    protected CircleCollider2D _CircleCollider;
    /// <summary>The smooth follow to track the hands back to the shoulder anchor points on the character they belong to</summary>
    protected SmoothFollow _SmoothFollow;

    /// <summary>The character stats to affect this hand</summary>
    protected CharacterStats _CharacterStats;
    /// <summary>The character tranform that owns this hand</summary>
    protected CharacterMainComponent _CharacterTransform;

    public int GetOwner() { return _CharacterTransform.photonView.owner.ID; }
    public CharacterMainComponent GetOwnerCharacter() { return _CharacterTransform; }

    public enum ThrowableState
    {
        IDLE,//Waitig to be thrown
        OUT,//Being thrown, on way to target
        RETURNING//Returning from target, back to the Character
    }

    /// <summary>The current state the hand is in, default is IDLE</summary>
    public ThrowableState CurrentThrownState = ThrowableState.IDLE;

    /// <summary>The current time spent being thrown OUT</summary>
    protected float CurrentTimeThrownOut;

    // Use this for initialization
    void Start()
    {
        _CircleCollider = GetComponent<CircleCollider2D>();
        _SmoothFollow = GetComponent<SmoothFollow>();
    }

    public void RegisterHandOwner(CharacterStats characterStats, CharacterMainComponent characterTransform)
    {
        _CharacterStats = characterStats;
        _CharacterTransform = characterTransform;
    }

    protected Vector3 _ThrownOutVector;

    /// <summary>If the current thrown state is in idle, then you are allowed to throw your hand</summary>
    /// <returns></returns>
    public bool CanThrowHand()
    {
        return CurrentThrownState == ThrowableState.IDLE;
    }

    /// <summary>Begin the throwing of the hand at the given directional vector</summary>
    /// <param name="directionalVector"></param>
    public void ThrowObject(Vector3 directionalVector)
    {
        //Cannot throw a hand if it isn't idle and ready to be thrown
        if (CurrentThrownState != ThrowableState.IDLE) return;
        CurrentThrownState = ThrowableState.OUT;

        _ThrownOutVector = directionalVector.normalized;
        _SmoothFollow.enabled = false;
        CurrentTimeThrownOut = 0f;

        if (!GetComponent<CircleCollider2D>().IsTouchingLayers(LayerConstants.ObstaclesLayerMask())) return;

        //If the master client says that this thrown hand should return, then start returning it
        if (!PhotonNetwork.isMasterClient) return;
        Collider2D[] cols = new Collider2D[1];
        GetComponent<CircleCollider2D>().GetContacts(new ContactFilter2D() { layerMask = LayerConstants.ObstaclesLayerMask(), useLayerMask = true }, cols);
        if (cols[0] == null) return;
        IAttackable attackable = cols.First().GetComponent<IAttackable>();
        PhotonView photonViewComp = cols.First().gameObject.GetComponent<PhotonView>();
        int attackablePhotonID = attackable == null || photonViewComp == null ? -1 : photonViewComp.viewID;
        _CharacterTransform.ThrowObjectCollided(attackablePhotonID, transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        switch (CurrentThrownState)
        {
            case ThrowableState.IDLE:
                break;
            case ThrowableState.OUT:
                transform.position += (_ThrownOutVector * _CharacterStats.CalculateHandThrownSpeedMultiplier()) * Time.deltaTime;
                CurrentTimeThrownOut += Time.deltaTime;
                if (CurrentTimeThrownOut < CharacterStats.BaseHandTimeOut) return;

                //If the master client says that this thrown hand should return, then start returning it
                if (!PhotonNetwork.isMasterClient) return;
                _CharacterTransform.RequestStartReturning(0);
                break;
            case ThrowableState.RETURNING:
                transform.position += ((_CharacterTransform.ProjectileAnchor.position - transform.position).normalized * _CharacterStats.CalculateHandThrownSpeedMultiplier()) * Time.deltaTime;
                if (Vector3.Distance(transform.position, _CharacterTransform.transform.position) > 2f) return;
                CurrentThrownState = ThrowableState.IDLE;
                _SmoothFollow.enabled = true;
                _SmoothFollow.smoothDampTime = _CharacterStats.BaseIdleHandDamperening;

                if (!PhotonNetwork.isMasterClient) return;
                _CharacterTransform.photonView.RPC("TriggerReturnedActions", PhotonTargets.All);
                break;
        }
    }

    /// <summary>Check to see if the hand is allowed to return yet</summary>
    /// <returns></returns>
    public bool CanStartReturning()
    {
        return CurrentThrownState == ThrowableState.OUT && CurrentTimeThrownOut > _CharacterStats.BaseHandTimeTilCanReturn;
        //If the hand isn't in the OUT state then it can't be returned
        //If you want to abide by the out time before being RETURNED then, then check to see if the current time out is more than the base amount of time required before a RETURN is allowed
    }

    /// <summary>Start the return of a ThrownHand
    /// If the ThrowHand is not in the OUT state then it will do nothing</summary>
    public void StartReturning(bool userForcedReturn, Vector2 pointReturnedFrom)
    {
        if (CurrentThrownState == ThrowableState.RETURNING) return;
        CurrentThrownState = ThrowableState.RETURNING;
        transform.position = pointReturnedFrom;

        if (!userForcedReturn) { ForEachAttachedThrowableItem(attachedItem => attachedItem.BeginReturn(this, pointReturnedFrom)); }
        else { ForEachAttachedThrowableItem(attachedItem => attachedItem.UserEarlyReturn(this, pointReturnedFrom)); }
    }

    /// <summary>For each item attached, pass it through this action and call a function on it</summary>
    /// <param name="itemAction"></param>
    protected void ForEachAttachedThrowableItem(Action<IThrowableItem> itemAction)
    {
        List<IThrowableItem> items = (List<IThrowableItem>)gameObject.GetAllAttachedTypes(typeof(IThrowableItem), true);
        items.AddRange((List<IThrowableItem>)_CharacterTransform.gameObject.GetAllAttachedTypes(typeof(IThrowableItem), true));
        foreach (IThrowableItem item in items)
        {
            itemAction.Invoke(item);
        }
    }

    /// <summary>Trigger all of the attachs actions for this hand, when the hand begins to be thrown</summary>
    [PunRPC]
    public void TriggerThrowActions(Vector2 pointThrownFrom)
    {
        ForEachAttachedThrowableItem(attachedItem => attachedItem.StartThrowing(this, pointThrownFrom));
    }

    /// <summary>Triggers all of the attached actions for this hand, for when the Hand Finished its return</summary>
    [PunRPC]
    public void TriggerReturnedActions(Vector2 pointReturnedTo)
    {
        ForEachAttachedThrowableItem(attachedItem => attachedItem.HandReturned(this, pointReturnedTo));
    }

    /// <summary>Trigger all of this attached actions for this hand, for when the hand has hit a target</summary>
    [PunRPC]
    public void TriggerAttackActions(IAttackable attackableHit, Vector2 attackPoint)
    {
        if (attackableHit != null) attackableHit.ApplyDamage(_CharacterTransform.BaseDamage);
        ForEachAttachedThrowableItem(attachedItem => attachedItem.HandAttack(this, attackableHit, attackPoint));
    }

    // ----------------------  COLLISION CALLS --------------------------------------

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (!PhotonNetwork.isMasterClient) return;
        if (CurrentThrownState == ThrowableState.IDLE) return;
        List<IAttackable> atks = ((List<IAttackable>)collision.collider.gameObject.GetAllAttachedTypes(typeof(IAttackable), true));
        PhotonView photonViewComp = collision.collider.gameObject.GetComponent<PhotonView>();
        int attackPhotonViewID = atks.FirstOrDefault() == null || photonViewComp == null ? -1 : photonViewComp.viewID;
        _CharacterTransform.ThrowObjectCollided(attackPhotonViewID, collision.contacts.Last().point);
    }
}
