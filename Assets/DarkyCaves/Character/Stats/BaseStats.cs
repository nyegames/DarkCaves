﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using SQLPlayerInfo;

public abstract class BaseStats : MonoBehaviour, IUpdateable
{
    /* ----------------------- HEALTH STUFF ------------------------- */

    /// <summary>The base value for this characters health</summary>
    public float BaseHealth = 400f;
    /// <summary>The current health of this unit</summary>
    /// <returns></returns>
    public abstract float CurrentHealth();

    /// <summary>A % resistance to all Crowd Controll effects, default 0%</summary>
    public float CCResistance = 0f;

    /* ----------------------- SHIELD STUFF ------------------------- */

    /// <summary>The base value for this characters shield</summary>
    public float BaseShield = 400f;

    /// <summary>The current shield of this unit</summary>
    /// <returns></returns>
    public abstract float CurrentShield();

    /// <summary>The time it will take for the shields to regen after they take damage</summary>
    public float TimeDelayBeforeShieldRegenFromDamage = 3f;

    /// <summary>The base movementspeed for this object</summary>
    public float BaseMovementSpeed = 50f;

    /* ----------------------- DAMAGE STUFF ------------------------- */

    /// <summary>The base damage value used </summary>
    public float BaseDamage = 200f;

    // Use this for initialization
    public abstract void Start();

    public float MinMovementSpeed = 20f;

    public float CalculateMovementSpeed()
    {
        return CalculateExtraMovementSpeed() + MinMovementSpeed;
    }

    /// <summary>
    /// Using equipment and a base movement speed, calculate how fast you are allowed to go
    /// </summary>
    /// <returns></returns>
    protected abstract float CalculateExtraMovementSpeed();

    ///// <summary>
    ///// Using player equipment and other affecting things, determine the time between the player making a footstep sound
    ///// </summary>
    ///// <returns></returns>
    //internal float SecondBetweenFootSteps()
    //{
    //    return 0.3f;
    //}

    ///// <summary>
    ///// Using the gear you have equipped, calculate the weight in total and convert it to the power each footstep will make in sound
    ///// </summary>
    ///// <returns></returns>
    //public float DetermineFootStepPower()
    //{
    //    float currentVel = Mathf.Abs(CharacterRigidBody2D.velocity.x + CharacterRigidBody2D.velocity.y);
    //    return 5f * currentVel;
    //}

    ///// <summary>
    ///// Using equipment weight values and the power of your footstep, determine the size which your footstep will travel
    ///// </summary>
    ///// <returns></returns>
    //internal float DetermineFootStepRadius()
    //{
    //    float value = DetermineFootStepPower() * 0.2f;
    //    return Mathf.Clamp(value, 1, 8);
    //}

    public abstract void IUpdate(float deltaTime);

    /// <summary>A diminishing returns calculation provided by;
    /// https://lostsouls.org/grimoire_diminishing_returns</summary>
    /// <param name="val"></param>
    /// <param name="scale"></param>
    /// <returns></returns>
    public static double diminishing_returns(double val, double scale)
    {
        if (val < 0) return -diminishing_returns(-val, scale);
        double mult = val / scale;
        double trinum = (Math.Sqrt(8.0 * mult + 1.0) - 1.0) / 2.0;
        return trinum * scale;
    }

}
