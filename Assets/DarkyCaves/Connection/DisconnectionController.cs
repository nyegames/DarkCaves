﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DisconnectionController : Photon.PunBehaviour
{
    public static DisconnectionController Instance;

    class PlayerInfo
    {
        public int PhotonID;
        public PhotonPlayer PhotonPlayer;
    }

    private Dictionary<PhotonPlayer, PlayerInfo> _PlayerGameInformation = new Dictionary<PhotonPlayer, PlayerInfo>();

    private

    // Use this for initialization
    void Start()
    {
        Instance = this;
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        PlayerInfo pInfo = _PlayerGameInformation[otherPlayer];
        _PlayerGameInformation.Remove(otherPlayer);
        PlayerController.Instance.RemovePlayer(otherPlayer.ID);

        if (!PhotonNetwork.isMasterClient) return;
        PhotonView pv = PhotonView.Find(pInfo.PhotonID);
        PhotonNetwork.Destroy(pv.gameObject);
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {

    }

    public override void OnCreatedRoom()
    {

    }

    public override void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {

    }

    [PunRPC]
    public void RegisterPlayerObject(int playerID, int photonID)
    {
        PhotonPlayer PPlayer = null;
        foreach (PhotonPlayer pp in PhotonNetwork.playerList) { if (pp.ID == playerID) { PPlayer = pp; break; } }

        if (PPlayer == null || _PlayerGameInformation.ContainsKey(PPlayer))
        {
            _PlayerGameInformation[PPlayer].PhotonID = photonID;
            _PlayerGameInformation[PPlayer].PhotonPlayer = PPlayer;
            return;
        }

        _PlayerGameInformation.Add(PPlayer, new PlayerInfo()
        {
            PhotonID = photonID,
            PhotonPlayer = PPlayer
        });
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }
}
