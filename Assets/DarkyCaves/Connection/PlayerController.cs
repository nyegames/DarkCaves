﻿using DarkyEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>The aim of the player controller is to handle the storage of all players available in the game
/// When players connect/disconnect the player controller is a static singleton and will be kept up to date by each master for all clients
/// on how many players are connected, and their information available</summary>
public class PlayerController : Photon.MonoBehaviour
{
    /// <summary>The name of the Prefab name at the end of the ResourcePlayerPrefabPath</summary>
    public string PlayerPrefabName = "My_Player";
    /// <summary>The path, from inside the Resource folder, of the prefab you want to use as your controllable player</summary>
    public string ResourcePlayerPrefabPath = "";
    /// <summary>Once your controllable player has been created, what do you want to call it in your local object Hiearchy</summary>
    public string OnceCreateMyPlayerName = "MyPlayer";

    public static PlayerController Instance;

    public CharacterControllerSwitcher CharacterControllerSwitch;

    public void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this);
    }

    /// <summary>Player information, 
    /// Key = PhotonPlayerID
    /// Value = PhotonViewID</summary>
    private Dictionary<int, int> _ActivePlayerInformation = new Dictionary<int, int>();

    public int ActivePlayerCount;

    public void Start()
    {

    }

    private void Update()
    {
        ActivePlayerCount = _ActivePlayerInformation.Count;
    }

    public void RemovePlayer(int photonPlayerID)
    {
        _ActivePlayerInformation.Remove(photonPlayerID);
    }


    /// <summary>Request a player from the Master Client PlayerController, pass in the photon players network id
    /// PhotonNetwork.player.ID</summary>
    /// <param name="playerID">The PhotonNetowk PhotonPlayer ID</param>
    [PunRPC]
    public void RequestPlayer(int playerID)
    {
        if (!PhotonNetwork.isMasterClient) return;
        string[] passiveArray = SQLPlayerInfo.SQLManager.Instance.GetActivePassives(PhotonNetwork.playerName).ToArray();
        string blinkAbility = SQLPlayerInfo.SQLManager.Instance.GetPlayerActiveBlink(PhotonNetwork.playerName);
        string[] fistingPowers = SQLPlayerInfo.SQLManager.Instance.GetPlayerActiveFistingPowers(PhotonNetwork.playerName).ToArray();
        AddPlayer(playerID, passiveArray, blinkAbility, fistingPowers);
    }

    /// <summary>The master client will create the player on the network
    /// Then tell all the other players that this player has been created, and it will send the PlayerID of the player who owns it</summary>
    /// <param name="pType">The type of player you want to create, this will be used to setup the base sprites</param>
    /// <param name="playerID">The PhotonNetowk PhotonPlayer ID</param>
    private void AddPlayer(int photonPlayerID, string[] passives, string blinkAbility, string[] fistingPowers)
    {
        GameObject player = PhotonNetwork.Instantiate(ResourcePlayerPrefabPath + PlayerPrefabName, Vector3.zero, Quaternion.identity, 0);
        player.name = PlayerPrefabName;
        PhotonView ppv = player.GetPhotonView();
        ppv.TransferOwnership(PhotonPlayer.Find(photonPlayerID));

        photonView.RPC("RegisterPlayerInfo", PhotonTargets.AllBuffered, photonPlayerID, ppv.viewID, passives, blinkAbility, fistingPowers);
    }

    /// <summary>The master player controller has created a player for you, and is now telling you that this player, owns this photonID</summary>
    /// <param name="pType">The type of player you want to create, this will be used to setup the base sprites</param>
    /// <param name="playerID">The PhotonNetowk PhotonPlayer ID</param>
    /// <param name="photonViewID">The PhotonNetwork PhotonViewID for the Player Object created</param>
    [PunRPC]
    public void RegisterPlayerInfo(int playerID, int photonViewID, string[] passives, string blinkAbility, string[] fistingPowers)
    {
        GameObject playerObj = PhotonView.Find(photonViewID).gameObject;
        LoadPlayerPassives(playerObj, passives);
        LoadPlayerBlinkAbility(playerObj, blinkAbility);
        LoadPlayerFistingAbilities(playerObj, fistingPowers);

        int oldPhotonViewID = -1;
        //If the player already exists, then just update the ActivePlayerInformation, this is used when you load into a mission level and are re-creating the player info
        if (!_ActivePlayerInformation.TryGetValue(playerID, out oldPhotonViewID)) _ActivePlayerInformation.Add(playerID, photonViewID);
        else _ActivePlayerInformation[playerID] = photonViewID;

        DisconnectionController.Instance.photonView.RPC("RegisterPlayerObject", PhotonTargets.AllBuffered, playerID, photonViewID);

        if (PhotonNetwork.player.ID != playerID) return;
        //If this is your player, then have the camera follow it and add the input controller scripts to it
        GameObject playerObject = PhotonView.Find(photonViewID).gameObject;
        Camera.main.GetComponent<SmoothFollow>().target = playerObject.transform;

        CharacterControllerSwitch = playerObject.AddComponent<CharacterControllerSwitcher>();
        CharacterControllerSwitch.InstallCharacterControllers = new List<CharacterControllerSwitcher.CharacterControllerTypes>
        {
            CharacterControllerSwitcher.CharacterControllerTypes.KeyboardMouse,
            CharacterControllerSwitcher.CharacterControllerTypes.Gamepad
        };
    }

    /// <summary>Update a current player with these details on all clients
    /// Removing all Passive, Blink and FistingPowers from the PlayerObject then add the new ones back</summary>
    /// <param name="playerID"></param>
    /// <param name="photonViewID"></param>
    /// <param name="passives"></param>
    /// <param name="blinkAbility"></param>
    /// <param name="leftFistingPowers"></param>
    /// <param name="rightFistingPowers"></param>
    [PunRPC]
    public void UpdatePlayerInfo(int playerID)
    {
        string[] passives = SQLPlayerInfo.SQLManager.Instance.GetActivePassives(PhotonNetwork.playerName).ToArray();
        string blinkAbility = SQLPlayerInfo.SQLManager.Instance.GetPlayerActiveBlink(PhotonNetwork.playerName);
        string[] fistingPowers = SQLPlayerInfo.SQLManager.Instance.GetPlayerActiveFistingPowers(PhotonNetwork.playerName).ToArray();
        int photonViewID = _ActivePlayerInformation[playerID];

        GameObject playerObj = PhotonView.Find(photonViewID).gameObject;

        List<Component> removeables = playerObj.GetComponents(typeof(Component)).ToList();
        foreach (Component comp in removeables) { if (comp is IPassive || comp is IOnBlink || comp is IThrowableItem) Destroy(comp); }

        GameObject[] hands = new GameObject[] { playerObj.GetComponent<CharacterMainComponent>().ThrowableGameObject };
        for (int hand = 0; hand < hands.Length; hand++)
        {
            List<Component> handRemoveables = hands[hand].GetComponents(typeof(Component)).ToList();
            foreach (Component comp in handRemoveables) { if (comp is IPassive || comp is IOnBlink || comp is IThrowableItem) Destroy(comp); }
        }

        LoadPlayerPassives(playerObj, passives);
        LoadPlayerBlinkAbility(playerObj, blinkAbility);
        LoadPlayerFistingAbilities(playerObj, fistingPowers);

        DarkyEvents.EventManager.TriggerEvent(EventNames.PlayerAbilitiesUpdated, passives, PhotonView.Find(photonViewID).isMine);

        DarkyEvents.EventManager.TriggerEvent(EventNames.HubPauseMenu, false, PhotonView.Find(photonViewID).isMine);
    }

    private void LoadPlayerPassives(GameObject playerObj, string[] passives)
    {
        foreach (string passiveName in passives)
        {
            Type passiveType = AbilityUtils.PassiveType(String.IsNullOrEmpty(passiveName) ? AbilityUtils.PassiveNames.DefaultWaitPassive : passiveName);
            playerObj.AddComponent(passiveType == null ? typeof(DefaultWaitPassive) : passiveType);
        }
    }

    private void LoadPlayerBlinkAbility(GameObject playerObj, string blinkAbility)
    {
        Type blinkType = AbilityUtils.BlinkType(blinkAbility);
        if (blinkAbility == null || String.IsNullOrEmpty(blinkAbility))
        {
            playerObj.AddComponent(AbilityUtils.BlinkType(AbilityUtils.BlinkNames.TeleportBlink));
            return;
        }
        playerObj.AddComponent(blinkType);
    }

    private void LoadPlayerFistingAbilities(GameObject playerObj, string[] fistingPowers)
    {
        CharacterMainComponent charMain = playerObj.GetComponent<CharacterMainComponent>();
        GameObject fist = charMain.ThrowableGameObject;

        List<string> powers = fistingPowers.ToList();

        foreach (string power in powers)
        {
            Type fistingPowerType = AbilityUtils.FistingPowerType(power);
            if (fistingPowerType == null) continue;
            fist.AddComponent(fistingPowerType);
        }
    }

    public GameObject GetPlayerObject(int photonPlayerID)
    {
        int photonID = 0;
        if (_ActivePlayerInformation.TryGetValue(photonPlayerID, out photonID))
        {
            return PhotonView.Find(photonID).gameObject;
        }
        return null;
    }

    [PunRPC]
    public void RequestLoadScene(string sceneName)
    {
        PhotonNetwork.isMessageQueueRunning = false;
        PhotonNetwork.LoadLevel(sceneName);
        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;

        photonView.RPC("RequestPlayer", PhotonTargets.AllBufferedViaServer, PhotonNetwork.player.ID);
    }

    private void SceneManager_activeSceneChanged(Scene arg0, Scene arg1)
    {
        SceneManager.activeSceneChanged -= SceneManager_activeSceneChanged;
        PhotonNetwork.isMessageQueueRunning = true;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }
}
