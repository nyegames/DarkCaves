﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadIntoRoomView : Photon.PunBehaviour
{
    public string SceneNameToLoadInto = "ViewRooms_Scene";

    // Use this for initialization
    void Start()
    {
        TryConnect();
    }

    private void TryConnect()
    {
        PlayerPrefs.DeleteKey("PUNCloudBestRegion");
        PhotonHandler.BestRegionCodeInPreferences = CloudRegionCode.eu;

        PhotonNetwork.BackgroundTimeout = 60f * 10f;
        PhotonNetwork.sendRate = 60;
        PhotonNetwork.sendRateOnSerialize = 60;
        PhotonNetwork.autoCleanUpPlayerObjects = false;
        PhotonNetwork.ConnectUsingSettings("0.1");
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("Connected to Lobby " + PhotonNetwork.lobby);
        PhotonNetwork.isMessageQueueRunning = false;
        UnityEngine.SceneManagement.SceneManager.LoadScene(SceneNameToLoadInto);
        UnityEngine.SceneManagement.SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
    }

    private void SceneManager_activeSceneChanged(UnityEngine.SceneManagement.Scene arg0, UnityEngine.SceneManagement.Scene arg1)
    {
        UnityEngine.SceneManagement.SceneManager.activeSceneChanged -= SceneManager_activeSceneChanged;
        PhotonNetwork.isMessageQueueRunning = true;
    }
}
