﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Data;
using Mono.Data.Sqlite;
using System.ComponentModel;

namespace SQLPlayerInfo
{
    public enum TableName
    {
        PlayerInformation,/// <summary>The name of the entire database</summary>
        Players,/// <summary>The name of the primary table, storing each of the players important information, this is where you get the PlayerID from</summary>
        UnlockedPassives,/// <summary>Name of the UnlockedPassives the player has earnt so far</summary>
        ActivePassives,/// <summary>The current passives the player has Active now</summary>
        UnlockedBlinks,/// <summary>Name of the UnlockedBlink abilities the player has earnt so far</summary>
        ActiveBlinks,/// <summary>The current blink ability the player has active now</summary>
        UnlockedFistingPowers,/// <summary>Name of the table which contains the names for all the powers used by the players hands, that they have unlocked</summary>
        ActiveFistingPowers/// <summary>Name of the table which contains the current equipped powers used by the players thrown hands</summary>
    }

    public class SQLManager : MonoBehaviour
    {
        /// <summary>Max amount of player passives allows to store int he DB</summary>
        public static int PlayerPassiveMax = 6;

        /// <summary>Current amount of Blinks being stored in the DB</summary>
        public static int PlayerBlinkMax = 1;

        public static string PlayerDatabaseName = "PlayerInformation";

        /// <summary>This has to be the path set for where the .sqlite database is you want to access</summary>
        public string ConnectionString;

        private static SQLManager _SQLManager;
        public static SQLManager Instance
        {
            get
            {
                if (_SQLManager == null)
                {
                    _SQLManager = FindObjectOfType(typeof(SQLManager)) as SQLManager;
                    if (_SQLManager == null) { Debug.LogError("There needs to be an EventManager somewhere in the scene"); }
                    else { _SQLManager.Init(); }
                }
                return _SQLManager;
            }
        }

        // Use this for initialization
        private void Init()
        {
            CreateDataBaseIfNotExist();
        }

        /// <summary>Create the information required to create a database table
        /// These will be the columns and their value types, eg.
        /// Passive0 TEXT
        /// Example of the itemName and its assosicated itemType</summary>
        /// <param name="amountItems"></param>
        /// <param name="itemNames"></param>
        /// <param name="itemTypes"></param>
        internal string TableInformation(int amountItems, string[] itemNames, string[] itemTypes)
        {
            string itemText = "";
            for (int i = 0; i < amountItems; i++)
            {
                string itemName = i > itemNames.Length - 1 ? itemNames[itemNames.Length - 1] : itemNames[i];
                string itemType = i > itemTypes.Length - 1 ? itemTypes[itemTypes.Length - 1] : itemTypes[i];

                itemText += String.Format("\"{0}\" {1}", itemName, itemType);
                if (i == amountItems - 1) break;
                itemText += ", ";
            }
            return itemText;
        }


        /* -------------------------------------------------------------------------------------------*/
        /* -------------------------------- SET AND UPDATE SQL INFORMATION -------------------------- */
        /* -------------------------------------------------------------------------------------------*/

        /// <summary>Will create the database "PlayerInformation" if it doesnt already exist</summary>
        private void CreateDataBaseIfNotExist()
        {
            string filePath = String.Format("{0}/{1}.sqlite", Application.persistentDataPath, PlayerDatabaseName);
            ConnectionString = String.Format("URI=file:{0};Version=3;", filePath);

            if (!System.IO.File.Exists(filePath))
            {
                SqliteConnection.CreateFile(filePath);
                using (SqliteConnection connection = new SqliteConnection(ConnectionString))
                {
                    connection.Open();

                    string playersTable = String.Format("CREATE TABLE \"{0}\" (\"PlayerID\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE , \"PlayerName\" TEXT UNIQUE)",
                    TableName.Players.ToString());
                    new SqliteCommand(playersTable, connection).ExecuteNonQuery();

                    /* ---------------- PASSIVE TABLES ---------------- */

                    string unlockedPassivesTable = String.Format("CREATE TABLE \"{0}\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE , \"PlayerID\" INTEGER , \"PassiveName\" TEXT)",
                    TableName.UnlockedPassives.ToString());
                    new SqliteCommand(unlockedPassivesTable, connection).ExecuteNonQuery();

                    string passiveText = TableInformation(SQLManager.PlayerPassiveMax,
                    new string[] { "Passive0", "Passive1", "Passive2", "Passive3", "Passive4", "Passive5", "Passive6", "Passive7", "Passive8", "Passive9", },
                    new string[] { "TEXT" });
                    string passiveTable = String.Format("CREATE TABLE \"{0}\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE , \"PlayerID\" INTEGER UNIQUE , {1})",
                        TableName.ActivePassives.ToString(), passiveText);
                    new SqliteCommand(passiveTable, connection).ExecuteNonQuery();


                    /* ------------------- BLINK TABLES ------------------- */

                    string unlockedBlinksTabke = String.Format("CREATE TABLE \"{0}\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE , \"PlayerID\" INTEGER , \"BlinkName\" TEXT)",
                    TableName.UnlockedBlinks.ToString());
                    new SqliteCommand(unlockedBlinksTabke, connection).ExecuteNonQuery();

                    string blinkText = TableInformation(SQLManager.PlayerBlinkMax, new string[] { "BlinkName" }, new string[] { "TEXT" });
                    string blinkTable = String.Format("CREATE TABLE \"{0}\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE , \"PlayerID\" INTEGER UNIQUE , {1})",
                        TableName.ActiveBlinks.ToString(), blinkText);
                    new SqliteCommand(blinkTable, connection).ExecuteNonQuery();

                    /* ----------------------- POWER FISTING TABLES --------------- */

                    string unlockedFistingPowers = String.Format("CREATE TABLE \"{0}\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE , \"PlayerID\" INTEGER , \"FistingPowerName\" TEXT)",
                    TableName.UnlockedFistingPowers.ToString());
                    new SqliteCommand(unlockedFistingPowers, connection).ExecuteNonQuery();

                    string activeFistingPowers = String.Format("CREATE TABLE \"{0}\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE , \"PlayerID\" INTEGER , \"FistingPowerName\" TEXT)",
                    TableName.ActiveFistingPowers.ToString());
                    new SqliteCommand(activeFistingPowers, connection).ExecuteNonQuery();

                    connection.Close();
                }
            }
        }

        /// <summary>Creates a new player with the name passed in</summary>
        /// <param name="playerName"></param>
        public void CreateNewPlayer(string playerName)
        {
            //Using statement automatiicaly disposes IDisposables
            using (SqliteConnection connection = new SqliteConnection(ConnectionString))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    //Create new blank player
                    if (!SQLPlayers.CheckPlayerExistsInDB(playerName))
                    {
                        CreateNewBlankPlayer(playerName, command);
                    }

                    UpdatePlayerInformation(SQLPlayers.GetPlayerID(playerName), null, null, command);

                    connection.Close();
                }
            }
        }

        /// <summary>Update the player information with the new passiveNames as their active passives and the blink name as its active blink
        /// If Acitve Passives/Blink are null, does not override information to blank</summary>
        /// <param name="playerID"></param>
        /// <param name="passiveNames"></param>
        /// <param name="blinkName"></param>
        /// <param name="command"></param>
        private void UpdatePlayerInformation(int playerID, List<string> passiveNames, string blinkName, IDbCommand command)
        {
            if (passiveNames != null && passiveNames.Count > 0)
            {
                string passiveValueList = " ";
                for (int i = 0; i < passiveNames.Count; i++)
                {
                    if (i >= SQLManager.PlayerPassiveMax) break;

                    passiveValueList += String.Format("Passive{0} = '{1}'", i, passiveNames[i]);
                    if (i == passiveNames.Count - 1) break;
                    passiveValueList += " ,";
                }

                string updateSQL = String.Format("UPDATE {0} SET {1} WHERE PlayerID = '{2}'", TableName.ActivePassives.ToString(), passiveValueList, playerID);
                command.CommandText = updateSQL;
                command.ExecuteScalar();
            }

            if (!String.IsNullOrEmpty(blinkName))
            {
                string updateBlink = String.Format("UPDATE {0} SET {1} WHERE PlayerID = '{2}'", TableName.ActiveBlinks.ToString(), blinkName, playerID);
                command.CommandText = updateBlink;
                command.ExecuteScalar();
            }
        }

        /// <summary>Create a Default Blank Player and populates their Unlocked Passives and Unlocked Blinks with the default Passives and Blinks</summary>
        /// <param name="playerName"></param>
        /// <param name="command"></param>
        private void CreateNewBlankPlayer(string playerName, IDbCommand command)
        {
            //Add new player to the Players table
            string addNewPlayer = String.Format("INSERT INTO {0} (PlayerName) VALUES ('{1}')", TableName.Players.ToString(), playerName);
            command.CommandText = addNewPlayer;
            command.ExecuteScalar();

            //Grab the playerID from the Players table
            int playerID = SQLPlayers.GetPlayerID(playerName);

            //Figure out the part of the state which is stating how many passives to set, if you are trying to set more than SQLManager.PlayerPassiveMax(6). Exit
            string passiveAmounts = "";
            string passiveComponentNames = "";
            for (int i = 0; i < SQLManager.PlayerPassiveMax; i++)
            {
                passiveAmounts += "Passive" + i.ToString();
                passiveComponentNames += "'" + "DefaultWaitPassive" + "'";

                if (i == SQLManager.PlayerPassiveMax - 1) break;
                passiveAmounts += ", ";
                passiveComponentNames += ", ";
            }

            string sqlPassiveTable = String.Format("INSERT INTO {0} (PlayerID, {1}) {2}", TableName.ActivePassives.ToString(), passiveAmounts, String.Format("VALUES('{0}', {1})", playerID, passiveComponentNames));
            command.CommandText = sqlPassiveTable;
            command.ExecuteScalar();

            string sqlBlinkTable = String.Format("INSERT INTO {0} (PlayerID, BlinkName) {1}",
                TableName.ActiveBlinks.ToString(), String.Format("VALUES('{0}', '{1}')", playerID, ""));
            command.CommandText = sqlBlinkTable;
            command.ExecuteScalar();

            //Add the 2 default passives a player starts with, DefaultWaitPassive and TeleportToUserReturnedHandPassive

            UnlockPassive(playerID, AbilityUtils.PassiveNames.DefaultWaitPassive);
            UnlockPassive(playerID, AbilityUtils.PassiveNames.TeleportToReturnHand);

            //Add the default SonicBoomOnBlink Blinkt ability
            UnlockBlink(playerID, AbilityUtils.BlinkNames.TeleportBlink);
            UnlockBlink(playerID, AbilityUtils.BlinkNames.TeleportSonicBoomLandBlink);

            UnlockFistingPower(playerID, AbilityUtils.FistingPowerNames.SonicExplosionAttackAction);
        }

        /// <summary>Add the passive name to the UnlockedPassive and assign its PlayerID to the included one</summary>
        /// <param name="playerID"></param>
        /// <param name="passiveName"></param>
        public void UnlockPassive(int playerID, string passiveName)
        {
            using (SqliteConnection connection = new SqliteConnection(ConnectionString))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    string playerName = SQLPlayers.GetPlayerName(playerID);
                    if (!SQLPlayers.CheckPlayerExistsInDB(playerName))
                    {
                        connection.Close();
                        return;
                    }

                    string sqlAddPassive = String.Format("INSERT INTO {0} (PlayerID, PassiveName) VALUES('{1}', '{2}')",
                        TableName.UnlockedPassives.ToString(), playerID, passiveName);
                    command.CommandText = sqlAddPassive;
                    command.ExecuteScalar();
                }
                connection.Close();
            }
        }

        /// <summary>Add this blinkName to the UnlockedBlinks for this PlayerID</summary>
        /// <param name="playerName"></param>
        /// <param name="blinkName"></param>
        public void UnlockBlink(int playerID, string blinkName)
        {
            using (SqliteConnection connection = new SqliteConnection(ConnectionString))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    string playerName = SQLPlayers.GetPlayerName(playerID);
                    if (!SQLPlayers.CheckPlayerExistsInDB(playerName))
                    {
                        connection.Close();
                        return;
                    }

                    string sqlAddPassive = String.Format("INSERT INTO {0} (PlayerID, BlinkName) VALUES('{1}', '{2}')",
                        TableName.UnlockedBlinks.ToString(), playerID, blinkName);
                    command.CommandText = sqlAddPassive;
                    command.ExecuteScalar();
                }
                connection.Close();
            }
        }

        /// <summary>Add a fisting power name to the UnlockedFistingPowers table</summary>
        /// <param name="playerID"></param>
        /// <param name="fistingPowername"></param>
        public void UnlockFistingPower(int playerID, string fistingPowername)
        {
            using (SqliteConnection connection = new SqliteConnection(ConnectionString))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    string playerName = SQLPlayers.GetPlayerName(playerID);
                    if (!SQLPlayers.CheckPlayerExistsInDB(playerName))
                    {
                        connection.Close();
                        return;
                    }

                    string sqlAddPassive = String.Format("INSERT INTO {0} (PlayerID, FistingPowerName) VALUES('{1}', '{2}')",
                        TableName.UnlockedFistingPowers.ToString(), playerID, fistingPowername);
                    command.CommandText = sqlAddPassive;
                    command.ExecuteScalar();
                }
                connection.Close();
            }
        }

        /// <summary>Returns a list of all players that are active in the database</summary>
        /// <returns></returns>
        internal IEnumerable<string> GetAllPlayers()
        {
            return SQLPlayers.GetAllPlayers();
        }

        /// <summary>Returns a list of all FistingPowers which a player has active, on the hands you specifiy</summary>
        /// <param name="playerName"></param>
        /// <param name="handName"></param>
        /// <returns></returns>
        internal IEnumerable<string> GetPlayerActiveFistingPowers(string playerName)
        {
            return SQLFistingPowers.GetPlayerActiveFistingPowers(playerName);
        }

        /// <summary>Returns all of the FistingPowers the player has access to</summary>
        /// <param name="playerName"></param>
        /// <returns></returns>
        internal IEnumerable<string> GetPlayerUnlockedFistingPowers(string playerName)
        {
            return SQLFistingPowers.GetPlayerUnlockedFistingPowers(playerName);
        }

        /// <summary>Add a FistingPower to the Players ActiveFistingPowers table</summary>
        /// <param name="playerName"></param>
        /// <param name="fistingPowerName"></param>
        /// <param name="handName"></param>
        internal void AddFistingPower(string playerName, string fistingPowerName)
        {
            SQLFistingPowers.AddFistingPower(playerName, fistingPowerName);
        }

        /// <summary>Remove a FistingPower from the Players ActiveFistingPowers table</summary>
        /// <param name="playerName"></param>
        /// <param name="fistingPowerName"></param>
        /// <param name="handName"></param>
        internal void RemoveFistingPower(string playerName, string fistingPowerName)
        {
            SQLFistingPowers.RemoveFistingPower(playerName, fistingPowerName);
        }


        /// <summary>Return list of all the BlinkAbilities available to the Player</summary>
        /// <param name="currentSelectedPlayerName"></param>
        /// <returns></returns>
        internal IEnumerable<string> GetPlayerUnlockedBlinks(string currentSelectedPlayerName)
        {
            return SQLBlink.GetPlayerUnlockedBlinks(currentSelectedPlayerName);
        }

        /// <summary>Updates the Player with the selected BlinkAbility name</summary>
        /// <param name="currentSelectedPlayerName"></param>
        /// <param name="blinkName"></param>
        /// <returns></returns>
        internal bool UpdateBlinkInfo(string currentSelectedPlayerName, string blinkName)
        {
            return SQLBlink.UpdateBlinkInfo(currentSelectedPlayerName, blinkName);
        }

        /// <summary>Returns the Players current Active BlinkAbility</summary>
        /// <param name="currentSelectedPlayerName"></param>
        /// <returns></returns>
        internal string GetPlayerActiveBlink(string currentSelectedPlayerName)
        {
            return SQLBlink.GetPlayerActiveBlink(currentSelectedPlayerName);
        }


        /// <summary>Return a list of all the PassiveAbilities this Player has access to</summary>
        /// <param name="playerName"></param>
        /// <returns></returns>
        internal IEnumerable<string> GetPlayerUnlockedPassives(string playerName)
        {
            return SQLPassives.GetPlayerUnlockedPassives(playerName);
        }

        /// <summary>Return a list of the current Active PassiveAbilities the Player has equip</summary>
        /// <param name="playerName"></param>
        /// <returns></returns>
        internal IEnumerable<string> GetActivePassives(string playerName)
        {
            return SQLPassives.GetActivePassives(playerName);
        }

        /// <summary>Updates the Active PassiveAbility slot with the current PassiveAbility</summary>
        /// <param name="currentSelectedPlayerName"></param>
        /// <param name="passiveName"></param>
        /// <param name="passiveIndexSelected"></param>
        /// <returns></returns>
        internal bool UpdatePassiveInfo(string currentSelectedPlayerName, string passiveName, int passiveIndexSelected)
        {
            return SQLPassives.UpdatePassiveInfo(currentSelectedPlayerName, passiveName, passiveIndexSelected);
        }
    }
}