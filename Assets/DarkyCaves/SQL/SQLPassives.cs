﻿using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UnityEngine;

namespace SQLPlayerInfo
{
    internal class SQLPassives
    {
        /// <summary>Returns the currently equipped passives for this player</summary>
        /// <param name="playerName"></param>
        /// <returns></returns>
        internal static IEnumerable<string> GetActivePassives(string playerName)
        {
            int playerID = SQLPlayers.GetPlayerID(playerName);

            string passivesToGet = "";
            for (int i = 0; i < SQLManager.PlayerPassiveMax; i++)
            {
                passivesToGet += "Passive" + i.ToString();
                if (i == SQLManager.PlayerPassiveMax - 1) break;
                passivesToGet += ", ";
            }

            string sqlQuery = string.Format("SELECT {0} FROM {1} WHERE PlayerID = \"{2}\"", passivesToGet, TableName.ActivePassives.ToString(), playerID);
            var querier = new SQLQuerier<string>(SQLManager.Instance.ConnectionString);
            return querier.QueryMultiple(sqlQuery, new StringResultReader()).Where(s=>!string.IsNullOrEmpty(s));
        }

        /// <summary>Returns the available passives that this player is able to equip</summary>
        /// <param name="playerName"></param>
        /// <returns></returns>
        internal static IEnumerable<string> GetPlayerUnlockedPassives(string playerName)
        {
            int playerID = SQLPlayers.GetPlayerID(playerName);

            string sqlQuery = String.Format("SELECT PassiveName FROM {0} WHERE PlayerID = \"{1}\"", TableName.UnlockedPassives.ToString(), playerID);
            var querier = new SQLQuerier<string>(SQLManager.Instance.ConnectionString);
            return querier.QueryMultiple(sqlQuery, new StringResultReader()).Where(s => !string.IsNullOrEmpty(s));
        }

        /// <summary>Update the current equipped passive, in the given passive index, which belongs to the given player</summary>
        /// <param name="playerName"></param>
        /// <param name="passiveAdded"></param>
        /// <param name="passiveIndex"></param>
        /// <returns></returns>
        internal static bool UpdatePassiveInfo(string playerName, string passiveAdded, int passiveIndex)
        {
            int playerID = SQLPlayers.GetPlayerID(playerName);
            //Create new blank player
            if (!SQLPlayers.CheckPlayerExistsInDB(playerName)) return false;

            //Using statement automatiicaly disposes IDisposables
            using (SqliteConnection connection = new SqliteConnection(SQLManager.Instance.ConnectionString))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    string passiveValue = string.Format("Passive{0} = '{1}'", passiveIndex, passiveAdded);

                    string updateSQL = String.Format("UPDATE {0} SET {1} WHERE PlayerID = '{2}'", TableName.ActivePassives.ToString(), passiveValue, playerID);
                    command.CommandText = updateSQL;
                    command.ExecuteScalar();
                }
                connection.Close();
            }
            return true;
        }
    }
}
