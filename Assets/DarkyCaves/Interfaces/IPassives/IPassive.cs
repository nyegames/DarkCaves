﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ThrowItem - Handles the events that happen when a hand is thrown
/// OnBlink - Handles the events that happen when a character blinks
/// Passive - Wants to know about both of these events, so it will have to copy behaviour from these classes, as well as implement its own abstract methods</summary>
public interface IPassive : IUpdateable, IThrowableItem, IOnBlink
{
    /// <summary>If this passive has met its conditions, then the next passive will be activated</summary>
    /// <returns>true if this passive has been completed</returns>
    bool PassiveCompleted();

    /// <summary>When a new passive is activated, this will be called to tell it so</summary>
    /// <param name="characterMain"></param>
    void OnPassiveActivate(CharacterMainComponent characterMain);

    /// <summary>When a new passive is activated, this will be called to tell you that this has been deactived</summary>
    /// <param name="characterMain"></param>
    void OnPassiveDeactivate(CharacterMainComponent characterMain);
}
