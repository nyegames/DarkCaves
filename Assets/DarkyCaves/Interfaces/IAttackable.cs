﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAttackable
{
    /// <summary>Applies this damage type</summary>
    /// <param name="damageType"></param>
    /// <returns>If this damage has killed this unit then return true</returns>
    bool ApplyDamage(DamageType damageType);
}
