﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportSonicBoomLandBlink : TeleportBlink
{
    private new float _BlinkDistance = 10f;

    public override float BlinkDistance
    {
        get { return _BlinkDistance; }

        protected set
        {
            base.BlinkDistance = value;
            _BlinkDistance = value;
        }
    }

    public override float BlinkCooldownSeconds { get { return 7f; } }

    public override void OnBlinkStarted(CharacterMainComponent characterTransform, Vector3 blinkedFrom)
    {
        GameObject explosion = (GameObject)Instantiate(Resources.Load("OnBlink/SonicExplosion_Animation"));
        explosion.GetComponent<ApplyDamageInCircleCollider>().RegisterAndExplode(characterTransform.photonView.ownerId, characterTransform.BaseDamage);
        explosion.transform.position = blinkedFrom;
    }
}
