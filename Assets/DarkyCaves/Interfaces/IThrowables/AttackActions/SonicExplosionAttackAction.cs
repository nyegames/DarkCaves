﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonicExplosionAttackAction : MonoBehaviour, IThrowableItem
{
    public float PowerPercentageConsumption { get { return 10f; } }

    public virtual float PercentOfBaseDamageUsed { get { return 50f; } }

    public void BeginReturn(ThrowableObject hand, Vector2 pointReturningFrom)
    {

    }

    public void HandAttack(ThrowableObject hand, IAttackable attackable, Vector2 attackedPoint)
    {
        GameObject explosion = (GameObject)Instantiate(Resources.Load("Explosions/SimpleExplosion_Animation"));
        explosion.transform.position = hand.transform.position;
        explosion.GetComponent<ApplyDamageInCircleCollider>().RegisterAndExplode(hand.GetOwner(), hand.GetOwnerCharacter().BaseDamage);
        if (attackable == null) return;
        DamageType dt = hand.GetOwnerCharacter().BaseDamage;
        dt.DamageValue *= PercentOfBaseDamageUsed > 0 ? (PercentOfBaseDamageUsed / 100f) : dt.DamageValue;
        attackable.ApplyDamage(dt);
    }

    public void HandReturned(ThrowableObject hand, Vector2 pointReturnedTo)
    {

    }

    public void StartThrowing(ThrowableObject hand, Vector2 pointThrownFrom)
    {

    }

    public void UserEarlyReturn(ThrowableObject hand, Vector2 pointReturningFrom)
    {

    }
}
