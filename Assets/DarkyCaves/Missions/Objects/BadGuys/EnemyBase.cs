﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HealthType))]
public abstract class EnemyBase : MonoBehaviour, IAttackable, IUpdateable
{
    public HealthType BadGuyHealth;

    public virtual bool ApplyDamage(DamageType damageType)
    {
        bool dead = BadGuyHealth.ApplyDamage(damageType);
        if (dead) HideAllButSprite();
        return dead;
    }

    protected virtual void Awake()
    {
        BadGuyHealth = GetComponent<HealthType>();
    }

    // Use this for initialization
    protected virtual void Start()
    {

    }

    public void IUpdate(float deltaTime)
    {

    }

    protected void HideAllButSprite()
    {
        foreach (Component comp in gameObject.GetComponentsInChildren<Component>())
        {
            if (comp is SpriteRenderer) continue;
            MonoBehaviour monoB = comp as MonoBehaviour;
            if (monoB != null) monoB.enabled = false;
            Collider2D col = comp as Collider2D;
            if (col != null) col.enabled = false;
        }
    }
}
