﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Static_Test_Enemy_00 : EnemyBase
{
    private SpriteRenderer _Sprite;
    private float _StartingScale;

    private CircleCollider2D _CirclCollider;
    private float _StartingRadius;

    public float StartingHealth = 300f;

    protected override void Start()
    {
        base.Start();
        _Sprite = GetComponentInChildren<SpriteRenderer>();
        _StartingScale = _Sprite.transform.localScale.x;
        _CirclCollider = GetComponentInChildren<CircleCollider2D>();
        _StartingRadius = _CirclCollider.radius;

        BadGuyHealth.RegisterHealthType(0, 0, StartingHealth);
    }

    public override bool ApplyDamage(DamageType damageType)
    {
        if (BadGuyHealth.Dead) return true;

        Color red = new Color(229 / 255f, 61 / 255f, 61 / 255f);
        //Value between 0-1
        float prop = BadGuyHealth.Health / StartingHealth;
        _Sprite.color = new Color((229 * prop) / 255f, (61 * prop) / 255, (61 * prop) / 255f);

        float currentScale = DarkyUtils.Lerp(_StartingScale, _StartingScale * 0.5f, prop);
        _Sprite.transform.localScale = new Vector3(currentScale, currentScale, 1f);

        float currentRadius = DarkyUtils.Lerp(_StartingRadius, _StartingRadius * 0.5f, prop);
        _CirclCollider.radius = currentRadius;

        bool dead = base.ApplyDamage(damageType);
        if (dead)
        {
            _Sprite.color = new Color(_Sprite.color.r, _Sprite.color.g, _Sprite.color.b, 0f);
        }
        return dead;
    }

}
